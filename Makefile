##
## EPITECH PROJECT, 2018
## Zappy
## File description:
## Makefile
##

CC		=	gcc

RM		=	rm -f

LN		=	ln -sf

NAME_SERVER	=	zappy_server

NAME_AI		=	zappy_ai

BINDIR		=	bin

DOCUMENTATION	=	documentation

all: libs $(NAME_SERVER) $(NAME_AI)

libs:
	$(MAKE) -C lib

$(NAME_SERVER): libs
	$(MAKE) -C $(NAME_SERVER)_proj
	$(LN) ../$(NAME_SERVER)_proj/$(NAME_SERVER) $(BINDIR)/
	$(LN) $(NAME_SERVER)_proj/$(NAME_SERVER) .

$(NAME_AI): libs
	$(MAKE) -C $(NAME_AI)_proj
	$(LN) ../$(NAME_AI)_proj/$(NAME_AI) $(BINDIR)
	$(LN) $(NAME_AI)_proj/$(NAME_AI) .

$(DOCUMENTATION):
	doxygen ./doxygen/doxy_conf

tests_run:
	$(MAKE) -C ./tests all

clean:
	$(MAKE) -C $(NAME_SERVER)_proj/ clean
	$(MAKE) -C $(NAME_AI)_proj/ clean
	$(MAKE) -C tests/ clean
	$(MAKE) -C lib/ clean

fclean:
	$(RM) $(BINDIR)/$(NAME_SERVER)
	$(RM) $(BINDIR)/$(NAME_AI)
	$(RM) $(NAME_SERVER)
	$(RM) $(NAME_AI)
	$(RM) $(NAME_AI)_proj/libutils.a
	$(RM) $(NAME_SERVER)_proj/libutils.a
	$(MAKE) -C $(NAME_SERVER)_proj/ fclean
	$(MAKE) -C $(NAME_AI)_proj/ fclean
	$(MAKE) -C tests/ fclean
	$(MAKE) -C lib/ fclean
	rm -rf ./doxygen/html
	rm -rf ./doxygen/latex

re: fclean all

.PHONY:	all libs tests_run clean fclean re
