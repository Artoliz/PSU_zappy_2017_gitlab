﻿using System;
using System.IO;
using System.Threading;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ManageGraphicalClient : MonoBehaviour {

	public int port = 6667;
	public string host = "10.15.192.251";
	public int width;
	public int height;

	private float Timer = 0;
	private int Frequence = 0;
	private int SaveX = -1;
	private int SaveY = -1;
	private NetworkStream ns;
	private TcpClient client;
	private ScaleObjects scale;
	private CreateTiles tiles;
	private ManageDrones drones;
	private Ressources ressources;
	private TeamsInfo teamsInfo;
	private StreamReader rd;
	private Dictionary<string, Func<int, int, bool>> fct = new Dictionary<string, Func<int, int, bool>>();

	void Start () {
		scale = GetComponent<ScaleObjects>();
		tiles = GetComponent<CreateTiles>();
		ressources = GetComponent<Ressources>();
		drones = GetComponent<ManageDrones>();
		teamsInfo = GetComponent<TeamsInfo>();
		InitDictionnaryOfInstantiateFunctions();
		ConnectToServer();
	}

	void Update () {
		string responses;

		WriteCommand("PING\n");
		responses = rd.ReadLine();
		ParseData(responses);
	}

	private void InitDictionnaryOfInstantiateFunctions() {
		fct["food"] = ressources.InstantiateBattery;
		fct["linemate"] = ressources.InstantiateLinemate;
		fct["sibur"] = ressources.InstantiateSibur;
		fct["deraumere"] = ressources.InstantiateDeraumere;
		fct["thystame"] = ressources.InstantiateThystame;
		fct["phiras"] = ressources.InstantiatePhiras;
		fct["mendiane"] = ressources.InstantiateMendiane;
	}

	private void InitTeamsInfo(string responses) {
		string[] command = responses.Split('|');
		string[] team;

		teamsInfo.NbTeams = 0;
		teamsInfo.Names = new string[command.Length];
		teamsInfo.Ids = new int[command.Length];
		for (int i = 0; i < command.Length; i++) {
			team = command[i].Split('\t');
			teamsInfo.Names[i] = team[0];
			int.TryParse(team[1], out teamsInfo.Ids[i]);
			teamsInfo.NbTeams += 1;
		}
	}

	private void InitMapInfo(string response) {
		string[] mapInfo = response.Split(' ');

		int.TryParse(mapInfo[0], out width);
		int.TryParse(mapInfo[1], out height);
		int.TryParse(mapInfo[2], out Frequence);
	}

	private void GetInfo() {
		string responses;

		responses = ReadResponses();
		WriteCommand("@GRAPHICAL\n");
		responses = ReadResponses();
		InitMapInfo(responses);
		responses = ReadResponses();
		InitTeamsInfo(responses);
		scale.ScaleTranslateObjects(width, height);
		tiles.CreateTilesMap(width, height);
	}

	public int ConnectToServer() {
		client = new TcpClient();
		try {
			client.Connect(host, port);
		} catch (SocketException e) {
			Debug.Log(e);
			Application.Quit();
		}
		ns = client.GetStream();
		rd = new StreamReader(ns);
		GetInfo();
		return 0;
	}

	private void FindFunction(string[] Ressource, int x, int y) {
		int index = x + (y * height);
		int NbRessources = 0;
		string[] tmp = Ressource[0].Split('-');

		if (tmp.Length == 2)
			drones.ManageDrone(tmp, Ressource[1], index);
		else {
			int.TryParse(Ressource[1], out NbRessources);
			if (NbRessources > 0) {
				fct[Ressource[0]].Invoke(NbRessources, index);
			}
		}
	}

	private void DestroySomeObjects(int CurrentIndex, int PreviousIndex) {
		string[] RessourcesName = {"Battery_", "Linemate_", "Deraumere_", "Sibur_", "Phiras_", "Thystame_", "Mendiane_"};
		int index = 0;
		int value = 0;
		GameObject tmp;

		for (int i = 0; i < RessourcesName.Length; i++) {
			index = PreviousIndex;
			for (; index < CurrentIndex; index++) {
				if (ressources.exist.TryGetValue(RessourcesName[i] + index, out value)) {
					tmp = GameObject.Find(RessourcesName[i] + index);
					Destroy(tmp);
					ressources.exist.Remove(RessourcesName[i] + index);
				}
			}
		}
	}

	private void ParseData(string response) {
		int x = 0;
		int y = 0;
		string[] Map = response.Split('|');
		string[] Cell;
		string[] Position;
		string[] Ressources;
		string[] Ressource;

		for (int i = 0; i < Map.Length - 1; i++) {
			Cell = Map[i].Split('=');
			Position = Cell[0].Split(';');
			Ressources = Cell[1].Split(',');
			int.TryParse(Position[0], out x);
			int.TryParse(Position[1], out y);
			//if (SaveX != -1 && SaveY != -1)
			//  DestroySomeObjects(x + (y * height), SaveX + (SaveY * height));
			//SaveX = x;
			//SaveY = y;
			for (int j = 0; j < Ressources.Length; j++) {
				Ressource = Ressources[j].Split(':');
				FindFunction(Ressource, x, y);
			}
		}
	}

	private string ReadResponses() {
		byte[] bytes_read = new byte[8192];

		ns.Read(bytes_read, 0, bytes_read.Length);
		return (Encoding.ASCII.GetString(bytes_read));
	}

	private void WriteCommand(string cmd) {
		byte[] bytes_write = Encoding.ASCII.GetBytes(cmd);

		// Timer += Time.deltaTime
		// if (Timer >= Frequence) {
		//      ns.Write(bytes_write, 0, bytes_write.Length);
		// }
		ns.Write(bytes_write, 0, bytes_write.Length);
	}
}