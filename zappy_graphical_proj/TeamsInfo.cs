﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamsInfo : MonoBehaviour {

	public string[] Names;
	public int[] Ids;
	public int NbTeams;

	public string GetTeamNameById(int Id) {
		string str = null;

		for (int i = 0; i < Ids.Length; i++) {
			if (Id == Ids[i]) {
				return (Names[i]);
			}
		}
		return (str);
	}
}
