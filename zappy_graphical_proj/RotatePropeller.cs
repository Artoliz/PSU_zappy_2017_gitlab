﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePropeller : MonoBehaviour {

	public int speed;

	void Start () {
		transform.eulerAngles = new Vector3 (-90, 0, 0);
	}

	void Update () {
		transform.Rotate (new Vector3(0, 0, speed * Time.deltaTime));		
	}
}
