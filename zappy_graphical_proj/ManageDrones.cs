﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageDrones : MonoBehaviour {

	public GameObject Drone;
	public GameObject Direction;
	private CreateTiles tiles;
	private ManageGraphicalClient Map;

	private DroneInfo SetDroneInfo(DroneInfo info, string[] Datas, GameObject Drone) {
		int dir = 0;
		GameObject tri;
		Quaternion rotation = Quaternion.identity;

		info.InitDirectionPosAndRot();
		int.TryParse(Datas[0], out dir);
		if ((tri = GameObject.Find(Drone.name + "/Direction(Clone)")) == null) {
			tri = Instantiate(Direction, Drone.transform);
		}
		tri.transform.localPosition = info.TrianglePos[dir];
		rotation.eulerAngles = info.TriangleRot[dir];
		tri.transform.rotation = rotation;
		int.TryParse(Datas[1], out info.Level);
		int.TryParse(Datas[2], out info.LifeTime);
		int.TryParse(Datas[3].Split('_')[1], out info.Food);
		int.TryParse(Datas[4].Split('_')[1], out info.Linemate);
		int.TryParse(Datas[5].Split('_')[1], out info.Deraumere);
		int.TryParse(Datas[6].Split('_')[1], out info.Sibur);
		int.TryParse(Datas[7].Split('_')[1], out info.Phiras);
		int.TryParse(Datas[8].Split('_')[1], out info.Thystame);
		int.TryParse(Datas[9].Split('_')[1], out info.Mendiane);
		return (info);
	}

	private void InstantiateDrone(int MapIndex, int DroneTeam, int DroneName, string Data) {
		GameObject tmp;
		DroneInfo info;
		Vector3 point = tiles.points[MapIndex];
		string Name;
		string[] Datas = Data.Split('/');

		point.y = 7.5f;
		Name = "Drone_" + DroneTeam + "_" + DroneName;
		tmp = Instantiate(Drone, point, new Quaternion(0, 0, 0, 0));
		tmp.name = Name;
		info = tmp.GetComponent<DroneInfo>();
		SetDroneInfo(info, Datas, tmp);
		info.TeamNumber = DroneTeam;
		info.Id = DroneName;
	}

	private int FindMapIndex(Vector3 DronePosition) {
		for (int i = 0; i < tiles.PointsSize; i++) {
			if (tiles.points[i].x == DronePosition.x
			&& tiles.points[i].z == DronePosition.z)
				return (i);
		}
		return (-1);
	}

	private void MoveDrone(int MapIndex, GameObject CurrentDrone, string Data) {
		DroneInfo info = CurrentDrone.GetComponent<DroneInfo>();
		string[] Datas = Data.Split('/');
		Vector3 point = tiles.points[MapIndex];
		int index = FindMapIndex(CurrentDrone.transform.position);

		Map = GameObject.Find("Client").GetComponent<ManageGraphicalClient>();
		SetDroneInfo(info, Datas, CurrentDrone);
		point.y = 7.5f;
		// Left To Right
		//if (index % Map.width == 0 && ((MapIndex + 1) % Map.width) == 0)
		//{
			//Vector3 Down = CurrentDrone.transform.position;
			//Vector3 Up = point;
			//Down.y = -5;
			//Up.y = -5;
			//CurrentDrone.transform.position = Vector3.MoveTowards(CurrentDrone.transform.position, Down, 0.5f);
			//Destroy(CurrentDrone);
			//CurrentDrone = Instantiate(Drone, Up, new Quaternion(0, 0, 0, 0));
			//SetDroneInfo(info, Datas, CurrentDrone);
			//CurrentDrone.transform.position = Vector3.MoveTowards(CurrentDrone.transform.position, point, 0.5f);
		//} else {
		CurrentDrone.transform.position = Vector3.MoveTowards(CurrentDrone.transform.position, point, 0.5f);
		//}
	}

	public void ManageDrone(string []DroneInfo, string Data, int MapIndex) {
		int DroneName = 0;
		int DroneTeam = 0;
		GameObject tmp;

		tiles = GameObject.Find("Client").GetComponent<CreateTiles>();
		int.TryParse(DroneInfo[0], out DroneTeam);
		int.TryParse(DroneInfo[1], out DroneName);
		tmp = GameObject.Find("Drone_" + DroneTeam + "_" + DroneName);
		if (tmp == null)
			InstantiateDrone(MapIndex, DroneTeam, DroneName, Data);
		else
			MoveDrone(MapIndex, tmp, Data);
	}
}