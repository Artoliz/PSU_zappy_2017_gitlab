﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayNbRessources : MonoBehaviour {

	public float LifeTime;

	private Ressources Res;
	private GameObject Cn;

	private void Start() {
		Cn = GameObject.Find(this.name + "/Canvas");
		Cn.SetActive(false);
	}

	private void OnMouseDown() {
		int value = 0;
		Text text;

		Res = GameObject.Find("Client").GetComponent<Ressources>();
		text = Cn.GetComponentInChildren<Text>();
		if (Res.exist.TryGetValue(this.name, out value)) {
			text.text = "X " + Res.exist[this.name];
		} else
			text.text = "X 0";
		Cn.SetActive(true);
		StartCoroutine(LateCall());
	}

	IEnumerator LateCall() {
		yield return new WaitForSeconds(LifeTime);

		Cn.SetActive(false);
	}
}
