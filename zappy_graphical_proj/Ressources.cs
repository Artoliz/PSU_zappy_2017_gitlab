﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ressources : MonoBehaviour {

	public GameObject Battery;
	public GameObject Linemate;
	public GameObject Sibur;
	public GameObject Deraumere;
	public GameObject Thystame;
	public GameObject Phiras;
	public GameObject Mendiane;
	public Dictionary<string, int> exist = new Dictionary<string, int>();
	private CreateTiles tiles;

	private void Start() {
		tiles = GameObject.Find("Client").GetComponent<CreateTiles>();
	}

	private void Update() {
		tiles = GameObject.Find("Client").GetComponent<CreateTiles>();
	}

	private int FindNbOfRessourcesAlreadyExisting(string ressourceName, int index) {
		int value = 0;

		if (!exist.TryGetValue(ressourceName + "_" + index, out value))
			return (0);
		return (value);
	}

	private void InstantiateObjects(string name, int NbRessources, int index, GameObject ToCreate, Vector3 point) {
		int nb = FindNbOfRessourcesAlreadyExisting(name, index);
		GameObject tmp;
		Quaternion rotation = Quaternion.identity;

		if (name == "Battery")
			rotation.eulerAngles = new Vector3(-90, 0, 0);
		else
			rotation.eulerAngles = new Vector3(0, 0, 0);
		if (nb == 0) {
			tmp = Instantiate(ToCreate, point, rotation);
			tmp.name = name + "_" + index;
		}
		exist[name + "_" + index] = NbRessources;
	}

	public bool InstantiateBattery(int NbRessource, int index) {
		Vector3 point = tiles.points[index];

		point.x -= 3;
		InstantiateObjects("Battery", NbRessource, index, Battery, point);
		return (true);
	}

	public bool InstantiateLinemate(int NbRessource, int index) {
		Vector3 point = tiles.points[index];

		InstantiateObjects("Linemate", NbRessource, index, Linemate, point);
		return (true);
	}

	public bool InstantiateSibur(int NbRessource, int index) {
		Vector3 point = tiles.points[index];

		point.z -= 3;
		InstantiateObjects("Sibur", NbRessource, index, Sibur, point);
		return (true);
	}

	public bool InstantiateDeraumere(int NbRessource, int index) {
		Vector3 point = tiles.points[index];

		point.z += 3;
		InstantiateObjects("Deraumere", NbRessource, index, Deraumere, point);
		return (true);
	}

	public bool InstantiateThystame(int NbRessource, int index) {
		Vector3 point = tiles.points[index];

		point.x += 3;
		InstantiateObjects("Thystame", NbRessource, index, Thystame, point);
		return (true);
	}

	public bool InstantiatePhiras(int NbRessource, int index) {
		Vector3 point = tiles.points[index];

		point.x += 3;
		point.z -= 3;
		InstantiateObjects("Phiras", NbRessource, index, Phiras, point);
		return (true);
	}

	public bool InstantiateMendiane(int NbRessource, int index) {
		Vector3 point = tiles.points[index];

		point.x += 3;
		point.z += 3;
		InstantiateObjects("Mendiane", NbRessource, index, Mendiane, point);
		return (true);
	}
}