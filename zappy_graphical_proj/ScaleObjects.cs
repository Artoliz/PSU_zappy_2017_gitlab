﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleObjects : MonoBehaviour
{
	public GameObject Trantor;
	public GameObject Cam;

	public void ScaleTranslateObjects(int width, int height) {
		width *= 10;
		height *= 10;
		Trantor.transform.localScale = new Vector3(width + 40, width, height + 40);
		Trantor.transform.position = new Vector3(width / 2.61f, -1 * (width / 2 + 2.7f), height / 2.94f);
		Cam.transform.position = new Vector3(Trantor.transform.position.x * 2,
				Trantor.transform.position.x, Trantor.transform.position.z);
	}
}