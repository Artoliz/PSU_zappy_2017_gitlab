﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateTiles : MonoBehaviour {

	public Vector3[] points;
	public int PointsSize;

	public void CreateTilesMap(int width, int height) {
		points = new Vector3[width * height];
		float a;
		float b;
	for (int x = 0; x < width; x++) {
			a = x * 10 + (width / 2);
			for (int y = 0; y < height; y++) {
				b = y * 10 + (height / 2);
				points [PointsSize] = new Vector3(a, 0.15f, b);
				PointsSize += 1;
			}
		}
	}
}
