/*
** EPITECH PROJECT, 2018
** PSU_ftrace_2017
** File description:
** my_list_destroy
*/

#include "list.h"

void list_destroy(list_t **node, int free_lvl, void (*fnc)(void *))
{
	list_t *list;
	list_t *bak;

	if (free_lvl <= LIST_FREE_BEG || free_lvl >= LIST_FREE_END)
		return;
	list = *node;
	while (list != NULL) {
		if (free_lvl == LIST_FREE_PTR) {
			free(list->elm);
			list->elm = NULL;
		}
		else if (free_lvl == LIST_FREE_FNC && fnc != NULL)
			(*fnc)(list->elm);
		bak = list->next;
		free(list);
		list = bak;
	}
	*node = NULL;
}
