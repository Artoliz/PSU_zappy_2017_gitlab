/*
** EPITECH PROJECT, 2018
** irc
** File description:
** irc
*/

#include "utils.h"

void utils_wt_destroy(char **wt)
{
	if (!wt)
		return;
	for (unsigned int it = 0; wt[it]; ++it)
		free(wt[it]);
	free(wt);
}