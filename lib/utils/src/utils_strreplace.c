/*
** EPITECH PROJECT, 2018
** myftp
** File description:
** myftp
*/

#include "utils.h"

static char *my_str_shiftright(char *str)
{
	int str_len = strlen(str);
	int it = str_len;

	if (str_len <= 0)
		return (str);
	while (it >= 0) {
		*(str + it + 1) = *(str + it);
		it--;
	}
	return (str);
}

static char *my_str_shiftleft(char *str)
{
	int	it = 0;
	int	str_len = strlen(str);

	if (str_len <= 0)
		return (str);
	while (it < str_len) {
		*(str + it) = *(str + it + 1);
		it++;
	}
	return (str);
}

static void replace_null(char *s, int len_f, const char *from)
{
	int it;

	s = strstr(s, from);
	while (s != NULL) {
		it = 0;
		while (it < len_f) {
			my_str_shiftleft(s);
			++it;
		}
		s = strstr(s, from);
	}
}

static void prepare_space(char *s, int len_f, int len_t)
{
	int it = 0;

	if (len_f == len_t)
		return;
	if (len_f > len_t) {
		while (len_t != len_f - it) {
			my_str_shiftleft(s);
			++it;
		}
	}
	else {
		while (len_f + it != len_t) {
			my_str_shiftright(s);
			++it;
		}
	}
}

bool utils_strreplace(char *s, const char *from, const char *to)
{
	int len_f;
	int len_t;

	len_f = strlen(from);
	if (to == NULL) {
		replace_null(s, len_f, from);
		return (strstr(s, from) != NULL);
	}
	len_t = strlen(to);
	s = strstr(s, from);
	if (s == NULL)
		return (false);
	prepare_space(s, len_f, len_t);
	strncpy(s, to, len_t);
	return (strstr(s, from) != NULL);
}