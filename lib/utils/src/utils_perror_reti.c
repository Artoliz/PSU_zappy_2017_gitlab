/*
** EPITECH PROJECT, 2018
** myftp
** File description:
** myftp
*/

#include "utils.h"

int utils_perror_reti(const char *s, int error)
{
	perror(s);
	return (error);
}