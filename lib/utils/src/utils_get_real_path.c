/*
** EPITECH PROJECT, 2018
** myftp
** File description:
** myftp
*/

#include "utils.h"

char *utils_get_real_path(char *curr_path, const char *new_path)
{
	char buffer[PATH_MAX + 1] = { 0 };
	char *ret;

	if (new_path[0] == '/')
		strncpy(curr_path, new_path, PATH_MAX);
	else if ((strlen(curr_path) + strlen(new_path) + 1) <= PATH_MAX) {
		strcat(curr_path, "/");
		strcat(curr_path, new_path);
	}
	else
		return (NULL);
	ret = realpath(strcpy(&buffer[0], curr_path), curr_path);
	if (ret == NULL && (errno == EACCES || errno == ENOENT))
		ret = &buffer[0];
	return (ret);
}