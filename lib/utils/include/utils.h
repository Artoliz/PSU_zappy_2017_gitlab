/*
** EPITECH PROJECT, 2018
** myftp.h
** File description:
** myftp.h
*/
/**
* \file
* \brief Utils header
*/

#ifndef UTIL_H_
# define UTIL_H_

#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/sysmacros.h>

bool utils_perror_ret(const char *s);
int utils_perror_reti(const char *s, int error);
bool utils_error_ret(const char *s);
int utils_error_reti(const char *s, int error);
char **utils_strsplit(const char *str, const char *delims, int max_delims);
char *utils_strjoin(char * const *wt, const char *separator);
size_t utils_wt_count(char * const *wt);
char **utils_wt_push(char **wt, const char *s);
char **utils_wt_copy(char * const *wt);
char **utils_wt_pop_front(char **wt, char **str);
void utils_wt_destroy(char **wt);
char *utils_get_real_path(char *curr_path, const char *new_path);
bool utils_strreplace(char *s, const char *from, const char *to);
char *utils_clearstr(char *s);

#endif /* !UTIL_H_ */
