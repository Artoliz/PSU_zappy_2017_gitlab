/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Unit Test retrieve_args.c
*/

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "ai.h"

static const char HELP[] = "USAGE: zappy_ai -p port -n name -h machine\n\n\t\
port\t\tis the port number\n\tname\t\tis the name of the team\n\tmachine\t\t\
is the name of the machine; localhost by default\n";

Test(test_ai_retrieve_args, test_ai_retrieve_args_help_message,
	.init=cr_redirect_stdout)
{
	const char *args[1] = {"-help"};
	ai_info_t info;

	ai_info_content_init(&info);
	cr_assert_eq(retrieve_args(1, args, &info), 0);
	fflush(stdout);
	cr_assert_stdout_eq_str(HELP);
	ai_info_content_destroy(&info);
}

Test(test_ai_retrieve_args, test_ai_retrieve_args_nb_args_error,
	.init=cr_redirect_stderr)
{
	cr_assert_eq(retrieve_args(5, NULL, NULL), -1);
	cr_assert_stderr_eq_str("zappy: Invalid number of argument.\n");
}

Test(test_ai_retrieve_args, test_ai_retrieve_args_no_args,
	.init=cr_redirect_stderr)
{
	cr_assert_eq(retrieve_args(6, NULL, NULL), -1);
	cr_assert_stderr_eq_str("zappy: Invalid number of argument.\n");
}

Test(test_ai_retrieve_args, test_ai_retrieve_args_no_info,
	.init=cr_redirect_stderr)
{
	const char *args[6] = {"-p", "4555", "12", "-h", "localhost", "-n"};

	cr_assert_eq(retrieve_args(6, args, NULL), -1);
	cr_assert_stderr_eq_str("zappy: Invalid number of argument.\n");
}

Test(test_ai_retrieve_args, test_ai_retrieve_args_inv_args_ord_no_args,
	.init=cr_redirect_stderr)
{
	const char *args[6] = {"-p", "4555", "12", "-h", "localhost", "-n"};
	ai_info_t info;

	ai_info_content_init(&info);
	cr_assert_eq(retrieve_args(6, args, &info), -1);
	cr_assert_stderr_eq_str("zappy: Invalid argument order.\nzappy: option -n is not followed by an argument.\n");
	ai_info_content_destroy(&info);
}

Test(test_ai_retrieve_args, test_ai_retrieve_args_inv_args_ord_double_opt, .init=cr_redirect_stderr)
{
	const char *args[6] = {"-p", "-n", "4555", "12", "-h", "localhost"};
	ai_info_t info;

	ai_info_content_init(&info);
	cr_assert_eq(retrieve_args(6, args, &info), -1);
	cr_assert_stderr_eq_str("zappy: Invalid argument order.\nzappy: option -p must be followed by an argument.\n");
	ai_info_content_destroy(&info);
}

Test(test_ai_retrieve_args, test_ai_retrieve_args_inv_port_form, .init=cr_redirect_stderr)
{
	const char *args[6] = {"-p", "qsdf154", "12", "-h", "localhost", "-n"};
	ai_info_t info;

	ai_info_content_init(&info);
	cr_assert_eq(retrieve_args(6, args, &info), -1);
	cr_assert_stderr_eq_str("zappy: option -p must be followed by an argument composed only by digits.\n");
	ai_info_content_destroy(&info);
}

Test(test_ai_retrieve_args, test_ai_retrieve_args_inv_port_val, .init=cr_redirect_stderr)
{
	const char *args[6] = {"-p", "154", "12", "-h", "localhost", "-n"};
	ai_info_t info;

	ai_info_content_init(&info);
	cr_assert_eq(retrieve_args(6, args, &info), -1);
	cr_assert_stderr_eq_str("zappy: port value must be over the reserved interval ( >= 1024).\n");
	ai_info_content_destroy(&info);
}

Test(test_ai_retrieve_args, test_ai_retrieve_inv_team, .init=cr_redirect_stderr)
{
	const char *args[6] = {"-p", "1540", "-h", "localhost", "-n", "@toto"};
	ai_info_t info;

	ai_info_content_init(&info);
	cr_assert_eq(retrieve_args(6, args, &info), -1);
	cr_assert_stderr_eq_str("zappy: option -n can't start by an at symbol (\"@\").\n");
	ai_info_content_destroy(&info);
}

Test(test_ai_retrieve_args, test_ai_retrieve_success)
{
	const char *args[6] = {"-p", "1540", "-h", "localhost", "-n", "toto"};
	ai_info_t info;

	ai_info_content_init(&info);
	cr_assert_eq(retrieve_args(6, args, &info), 0);
	cr_assert_str_eq(info.team, "toto");
	cr_assert_str_eq(info.connection->hostname, "localhost");
	cr_assert_eq(info.connection->port, 1540);
	ai_info_content_destroy(&info);
}