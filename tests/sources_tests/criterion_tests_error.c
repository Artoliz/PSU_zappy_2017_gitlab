/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Unit Test Error.c
*/

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "error.h"

Test(test_error, test_display_error_and_exit)
{
	pid_t cpid = 0;
	int status = 0;

	cpid = fork();
	if (cpid == -1) {
		perror("Error fork()");
		exit(84);
	}
	if (cpid == 0)
		display_error_and_exit("Je suis l'erreur a afficher.", 84);
	else
		wait(&status);
	if (WIFEXITED(status))
		cr_assert_eq(WEXITSTATUS(status), 84);
}