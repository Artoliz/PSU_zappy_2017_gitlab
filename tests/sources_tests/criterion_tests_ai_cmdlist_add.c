/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Unit Test ai_cmdlist.c
*/

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include <criterion/parameterized.h>
#include <string.h>
#include "ai.h"

static const char STR_REF[] = "Coucou les copains";

Test(test_ai_cmdlist_add, test_ai_cmdlist_add_null)
{
	cr_assert_eq(ai_cmdlist_add_element(NULL), -1);
}

Test(test_ai_cmdlist_add, test_ai_cmdlist_add_hundred_of_string)
{
	for (int i = 0; i < 100; ++i)
		cr_assert_eq(ai_cmdlist_add_element(STR_REF), 1);
	for (int i = 0; i < 100; ++i) {
		cr_assert_str_eq(ai_cmdlist(NULL)[i], STR_REF);
		free(ai_cmdlist(NULL)[i]);
	}
	free(ai_cmdlist(NULL));
}