/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Unit Test ai_cmdlist.c
*/

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include <criterion/parameterized.h>
#include <string.h>
#include "ai.h"
#include "buffer.h"

Test(test_ai_buffer, test_ai_buffer_get_first)
{
	buffer_t *tmp = NULL;

	tmp = ai_buffer_get();
	cr_assert_not_null(tmp);
	cr_assert_str_empty(tmp->buf);
	cr_assert_eq(tmp->r, 0);
	cr_assert_eq(tmp->w, 0);
	cr_assert_eq(tmp->size, RECV_SIZE);
}

Test(test_ai_buffer, test_ai_buffer_get_multiple)
{
	buffer_t *tmp = NULL;
	buffer_t *tmp2 = NULL;

	tmp = ai_buffer_get();
	cr_assert_not_null(tmp);
	cr_assert_str_empty(tmp->buf);
	cr_assert_eq(tmp->r, 0);
	cr_assert_eq(tmp->w, 0);
	cr_assert_eq(tmp->size, RECV_SIZE);
	strcpy(tmp->buf, "Coucou les copains\n");
	tmp->w = 20;
	tmp->r = 18;
	tmp2 = ai_buffer_get();
	cr_assert_not_null(tmp2);
	cr_assert_str_eq(tmp2->buf, "Coucou les copains\n");
	cr_assert_eq(tmp->r, 18);
	cr_assert_eq(tmp->w, 20);
	cr_assert_eq(tmp->size, RECV_SIZE);
}

Test(test_ai_buffer, test_ai_buffer_add_input_incomplete_input)
{
	buffer_t *tmp = NULL;
	char **wt = NULL;

	tmp = ai_buffer_get();
	cr_assert_not_null(tmp);
	cr_assert_eq(ai_buffer_add_input(tmp, "Coucou les copains", 18), 0);
	cr_assert_str_eq(tmp->buf, "Coucou les copains");
	cr_assert_eq(tmp->r, 0);
	cr_assert_eq(tmp->w, 18);
	wt = ai_cmdlist(NULL);
	cr_assert_null(wt);
}

Test(test_ai_buffer, test_ai_buffer_add_input_completeting_input)
{
	buffer_t *tmp = NULL;
	char **wt = NULL;

	tmp = ai_buffer_get();
	cr_assert_not_null(tmp);
	cr_assert_eq(ai_buffer_add_input(tmp, "Coucou les copains\n", 19), 1);
	cr_assert_str_eq(tmp->buf, "Coucou les copains\n");
	cr_assert_eq(tmp->r, 19);
	cr_assert_eq(tmp->w, 19);
	wt = ai_cmdlist(NULL);
	cr_assert_not_null(wt);
	cr_assert_str_eq(wt[0], "Coucou les copains");
}

Test(test_ai_buffer, test_ai_buffer_add_input_multiple_inputs)
{
	buffer_t *tmp = NULL;
	char **wt = NULL;

	tmp = ai_buffer_get();
	cr_assert_not_null(tmp);
	cr_assert_eq(ai_buffer_add_input(tmp,
		"Coucou les copains\nComment ça va?\nQuoi de neuf?\n", 49), 3);
	cr_assert_str_eq(tmp->buf,
		"Coucou les copains\nComment ça va?\nQuoi de neuf?\n");
	cr_assert_eq(tmp->r, 49);
	cr_assert_eq(tmp->w, 49);
	wt = ai_cmdlist(NULL);
	cr_assert_not_null(wt);
	cr_assert_str_eq(wt[0], "Coucou les copains");
	cr_assert_str_eq(wt[1], "Comment ça va?");
	cr_assert_str_eq(wt[2], "Quoi de neuf?");
}

Test(test_ai_buffer, test_ai_buffer_add_input_overload_buffer)
{
	int oversize = RECV_SIZE * 2;
	char oversized[oversize];
	char temp[oversize];
	buffer_t *tmp = NULL;
	char **wt = NULL;

	memset(oversized, 0, oversize);
	memset(temp, 0, oversize);
	memset(oversized, '0', RECV_SIZE + 15);
	tmp = ai_buffer_get();
	cr_assert_not_null(tmp);
	cr_assert_eq(ai_buffer_add_input(tmp, oversized, RECV_SIZE + 15), 0);
	strncpy(temp, tmp->buf, RECV_SIZE);
	temp[RECV_SIZE] = 0;
	cr_assert_str_eq(temp, oversized + 15);
	cr_assert_eq(tmp->r, 16);
	cr_assert_eq(tmp->w, 15);
	wt = ai_cmdlist(NULL);
	cr_assert_null(wt);
}

Test(test_ai_buffer, test_ai_buffer_add_input_circum_buffer)
{
	int oversize = RECV_SIZE * 2;
	char oversized[oversize];
	char temp[oversize];
	buffer_t *tmp = NULL;
	char **wt = NULL;
	int ret = 0;

	memset(oversized, 0, oversize);
	memset(temp, 0, oversize);
	memset(oversized, '0', RECV_SIZE - 2);
	oversized[RECV_SIZE - 2] = '\n';
	tmp = ai_buffer_get();
	cr_assert_not_null(tmp);
	cr_assert_eq(ai_buffer_add_input(tmp, "Coucou les copains\n", 19), 1);
	ret = ai_buffer_add_input(tmp, oversized, RECV_SIZE - 1);
	strncpy(temp, tmp->buf, RECV_SIZE);
	temp[RECV_SIZE] = 0;
	cr_assert_eq(ret, 1);
	cr_assert_eq(tmp->r, 18);
	cr_assert_eq(tmp->w, 18);
	wt = ai_cmdlist(NULL);
	cr_assert_not_null(wt);
	oversized[RECV_SIZE - 2] = 0;
	cr_assert_str_eq(wt[0], "Coucou les copains");
	cr_assert_str_eq(wt[1], oversized);
}

Test(test_ai_buffer, test_ai_buffer_add_input_circum_buffer_2_inputs)
{
	int oversize = RECV_SIZE * 2;
	char oversized[oversize];
	char temp[oversize];
	buffer_t *tmp = NULL;
	char **wt = NULL;
	int ret = 0;

	memset(oversized, 0, oversize);
	memset(temp, 0, oversize);
	memset(oversized, '0', RECV_SIZE - 2);
	oversized[RECV_SIZE / 2] = '\n';
	oversized[RECV_SIZE - 2] = '\n';
	tmp = ai_buffer_get();
	cr_assert_not_null(tmp);
	cr_assert_eq(ai_buffer_add_input(tmp, "Coucou les copains\n", 19), 1);
	ret = ai_buffer_add_input(tmp, oversized, RECV_SIZE - 1);
	strncpy(temp, oversized, RECV_SIZE/2);
	temp[RECV_SIZE/2] = 0;
	cr_assert_eq(ret, 2);
	cr_assert_eq(tmp->r, 18);
	cr_assert_eq(tmp->w, 18);
	wt = ai_cmdlist(NULL);
	cr_assert_not_null(wt);
	oversized[RECV_SIZE - 2] = 0;
	cr_assert_str_eq(wt[0], "Coucou les copains");
	cr_assert_str_eq(wt[1], temp);
	cr_assert_str_eq(wt[2], oversized + RECV_SIZE / 2 + 1);
}