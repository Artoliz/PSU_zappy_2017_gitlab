/*
** EPITECH PROJECT, 2018
** tests
** File description:
** tests
*/

#include <criterion/criterion.h>
#include "player.h"

Test(test_player, player_commands)
{
	player_t player;
	player_command_t *cmd;

	memset(&player, 0, sizeof(player_t));
	cmd = player_push_command(&player, 10, (char *[]) { "hello", NULL });
	cr_assert_neq(cmd, NULL);
	player_pop_command(&player, cmd);
}

