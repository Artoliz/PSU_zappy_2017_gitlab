/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** zappy
*/

#include <criterion/criterion.h>
#include "network.h"

Test(test_map, map_common)
{
	server_t server;
	map_t map;
	args_t args;

	memset(&server, 0, sizeof(server_t));
	memset(&args, 0, sizeof(args_t));

	args.width = 5;
	args.height = 5;
	cr_assert_neq(map_init(&args, &map), false);
	cr_assert_eq(strcmp(
		map_name_by_resource(RESOURCE_LINEMATE), "linemate"), 0);
}