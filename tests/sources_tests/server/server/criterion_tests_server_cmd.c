/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** zappy
*/

#include <criterion/criterion.h>
#include "network.h"
#include "client.h"
#include "server.h"

Test(test_server, server_cmds)
{
	server_t server;
	client_t *client = calloc(1, sizeof(client_t));
	client_t *client2 = calloc(1, sizeof(client_t));
	team_t *team = calloc(1, sizeof(team_t));
	map_t *map = calloc(1, sizeof(map_t));
	args_t args;

	memset(&server, 0, sizeof(server_t));
	memset(&args, 0, sizeof(args_t));
	args.width = 5;
	args.height = 5;
	cr_assert_eq(map_init(&args, map), true);
	cr_assert_eq(player_init(&client->player, map, team), true);
	cr_assert_eq(player_init(&client2->player, map, team), true);
	cr_assert_neq(server_init(&args, &server), false);
	client_init(client);
	client_init(client2);
	cr_assert_neq(list_push(&server.clients, client), false);
	cr_assert_neq(list_push(&server.clients, client2), false);
	cr_assert_neq(list_push(&server.teams, team), false);
	client->greeted = true;
	client2->greeted = true;
	server_cmd_forward(&server, client, NULL);
	server_cmd_inventory(&server, client, NULL);
	server_cmd_left(&server, client, NULL);
	server_cmd_left(&server, client, NULL);
	server_cmd_left(&server, client, NULL);
	server_cmd_left(&server, client, NULL);
	server_cmd_right(&server, client, NULL);
	server_cmd_right(&server, client, NULL);
	server_cmd_right(&server, client, NULL);
	server_cmd_right(&server, client, NULL);
	server_cmd_connect_nbr(&server, client, NULL);
	server_cmd_eject(&server, client, NULL);
	server_cmd_fork(&server, client, NULL);
	server_cmd_incantation(&server, client, NULL);
	server_cmd_look(&server, client, NULL);
	server_cmd_set(&server, client, (char *[]) { "", "food", NULL });
	server_cmd_take(&server, client, (char *[]) { "", "food", NULL });
	server_cmd_set(&server, client, (char *[]) { "", "aaa", NULL });
	server_cmd_take(&server, client, (char *[]) { "", "aaa", NULL });
	server_cmd_set(&server, client, (char *[]) { "", NULL, NULL });
	server_cmd_take(&server, client, (char *[]) { "", NULL, NULL });
	server_cmd_broadcast(&server, client, (char *[]) { "", "lel", NULL });
}