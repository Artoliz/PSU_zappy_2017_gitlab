/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** zappy
*/

#include <criterion/criterion.h>
#include "network.h"
#include "server.h"

Test(test_client, server_common)
{
	server_t server;
	map_t map;
	client_t *client = calloc(1, sizeof(client_t));
	team_t *team = calloc(1, sizeof(team_t));
	args_t args;

	memset(&server, 0, sizeof(server_t));
	memset(&args, 0, sizeof(args_t));

	args.width = 5;
	args.height = 5;
	cr_assert_neq(map_init(&args, &map), false);
	map_init_tiles(&map, 10);
	player_init(&client->player, &map, team);
	cr_assert_eq(server_init(&args, &server), true);
	cr_assert_neq(list_push(&server.clients, client), false);
	cr_assert_neq(list_push(&server.teams, team), false);
	server_load_args(&args, &server);
	server_destroy(&server);
}