/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** zappy
*/

#include <criterion/criterion.h>
#include "network.h"
#include "client.h"

Test(test_client, client_common)
{
	client_t client;
	FILE *file = tmpfile();
	char buf[128] = { 0 };

	cr_assert_neq(file, NULL);
	client_init(&client);
	client.data.fd = fileno(file);
	cr_assert_eq(client_sendmsg(&client, "Hello World"), true);
	fseek(file, 0, SEEK_SET);
	read(client.data.fd, &buf[0], 127);
	cr_assert_eq(strcmp(&buf[0], "Hello World\n"), 0);
	fclose(file);
	cr_assert_eq(client_sendmsg(&client, "Hello World"), false);
	client.connected = false;
	cr_assert_eq(client_sendmsg(&client, "Hello World"), false);
}