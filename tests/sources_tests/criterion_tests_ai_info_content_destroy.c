/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Unit Test ai_info_content_destroy.c
*/

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "ai.h"

Test(test_ai_info_destroy, test_ai_info_content_destroy_no_data, .init=cr_redirect_stderr)
{
	cr_assert_eq(ai_info_content_destroy(NULL), -1);
	cr_assert_stderr_eq_str("zappy: ai_info_destroy: Invalid ptr.\n");
}

Test(test_ai_info_destroy, test_ai_info_content_destroy_success_no_connection)
{
	ai_info_t info = {NULL, NULL};

	cr_assert_eq(ai_info_content_destroy(&info), 0);
	cr_assert_null(info.team);
	cr_assert_null(info.connection);
}

Test(test_ai_info_destroy, test_ai_info_content_destroy_success)
{
	ai_info_t info = {NULL, NULL};

	cr_assert_eq(ai_info_content_init(&info), 0);
	cr_assert_null(info.team);
	cr_assert_not_null(info.connection);
	cr_assert_eq(info.connection->port, 0);
	cr_assert_null(info.connection->hostname);
	cr_assert_eq(ai_info_content_destroy(&info), 0);
	cr_assert_null(info.team);
	cr_assert_null(info.connection);
}