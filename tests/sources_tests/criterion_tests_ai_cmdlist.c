/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Unit Test ai_cmdlist.c
*/

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include <criterion/parameterized.h>
#include <string.h>
#include "ai.h"

struct my_params
{
	char **params;
};

void cleanup_ai_cmdlist_params(struct criterion_test_params *ctp)
{
	cr_free(((struct my_params *)ctp->params)->params[0]);
	cr_free(((struct my_params *)ctp->params)->params);
}

ParameterizedTestParameters(test_ai_cmdlist, test_ai_cmdlist_assign_smth_access_smth)
{
	static struct my_params params[] = {{NULL}};

	params->params = cr_malloc(sizeof(char *) * 2);
	params->params[0] = cr_malloc(sizeof(char *) * 15);
	memset(params->params[0], 0, 15);
	strcpy(params->params[0], "Coucou");
	params->params[1] = NULL;
	return (cr_make_param_array(struct my_params, params, 1, cleanup_ai_cmdlist_params));
}

ParameterizedTestParameters(test_ai_cmdlist, test_ai_cmdlist_assign_smth)
{
	static struct my_params params[] = {{NULL}};

	params->params = cr_malloc(sizeof(char *) * 2);
	params->params[0] = cr_malloc(sizeof(char *) * 15);
	memset(params->params[0], 0, 15);
	strcpy(params->params[0], "Coucou");
	params->params[1] = NULL;
	return (cr_make_param_array(struct my_params, params, 1, cleanup_ai_cmdlist_params));
}

Test(test_ai_cmdlist, test_ai_cmdlist_assign_null)
{
	char **array = NULL;

	cr_assert_null(ai_cmdlist(array));
}

ParameterizedTest(struct my_params *params, test_ai_cmdlist, test_ai_cmdlist_assign_smth)
{
	cr_assert_not_null(ai_cmdlist(params->params));
}

ParameterizedTest(struct my_params *params, test_ai_cmdlist, test_ai_cmdlist_assign_smth_access_smth)
{
	cr_assert_str_eq(params->params[0], "Coucou");
	cr_assert_not_null(ai_cmdlist(params->params));
	cr_assert_str_eq(ai_cmdlist(NULL)[0], params->params[0]);
}

Test(test_ai_cmdlist, test_ai_cmdlist_assign_nothg_access_nothg)
{
	char **array = NULL;

	cr_assert_null(ai_cmdlist(array));
	cr_assert_null(ai_cmdlist(NULL));
}