/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Unit Test ai_help.c
*/

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "ai.h"

Test(test_ai_help, test_ai_help_success, .init=cr_redirect_stdout)
{
	cr_assert_eq(display_ai_help_message("zappy"), 0);
	fflush(stdout);
	cr_assert_stdout_eq_str("USAGE: zappy -p port -n name -h machine\n\n\tport\t\tis the port number\n\tname\t\tis the name of the team\n\tmachine\t\tis the name of the machine; localhost by default\n");
}