/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Unit Test ai_info_content_init.c
*/

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "ai.h"

Test(test_ai_info_init, test_ai_info_content_init_no_data, .init=cr_redirect_stderr)
{
	cr_assert_eq(ai_info_content_init(NULL), -1);
	cr_assert_stderr_eq_str("zappy: ai_info_init: Invalid ptr.\n");
}

Test(test_ai_info_init, test_ai_info_content_init_success)
{
	ai_info_t info = {NULL, NULL};

	cr_assert_eq(ai_info_content_init(&info), 0);
	cr_assert_null(info.team);
	cr_assert_not_null(info.connection);
	cr_assert_eq(info.connection->port, 0);
	cr_assert_null(info.connection->hostname);
}