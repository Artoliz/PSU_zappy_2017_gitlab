#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include "ai.h"

int main(int ac, const char **av)
{
	int ret = 1;

	if (main_connection(ac, av))
		return (-1);
	while (ret > 0) {
		ret = is_available();
		if (ret > 0 && (ret ^ WR_MASK) > 0)
			printf("%s\n", ai_cmdlist_pop_element());
	}
	return (0);
}
