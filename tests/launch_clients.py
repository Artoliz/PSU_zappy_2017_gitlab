#!/usr/bin/env python3.6

import socket
import sys
import argparse
import signal
import time

#List of all the clients
clients = []

class Client:

	def __init__(self, host, port, team):
		self.host = host
		self.port = port
		self.team = team

	def __connect_to_server__(self):
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.connect((self.host, self.port))
		self.socket.send(self.team.encode())

	def __send_action__(self, action):
		self.socket.send(action)

	def __close_connection__(self):
		self.socket.close()


def signal_handler(signal, frame):
	for client in clients:
		client.__close_connection__()
	exit(0)


def main():
	parser = argparse.ArgumentParser()
	signal.signal(signal.SIGINT, signal_handler)

	parser.add_argument("addr", help="The host's address.", type=str)
	parser.add_argument("port", help="The host's port.", type=int)
	parser.add_argument("player_nb",
			help="The number of players in each team.", type=int)
	parser.add_argument("teams", help="The teams' names.", nargs='+',
				type=str)
	args = parser.parse_args()
	for team in args.teams:
		for i in range(0, args.player_nb):
			clients.append(Client(
				args.addr,
				args.port,
				team))
	for client in clients:
		client.__connect_to_server__()
	while True:
		time.sleep(1)
	return 0

main()