/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Client header
*/

#ifndef CLIENT_H_
# define CLIENT_H_

#include "zappy_server.h"
#include "network.h"

void client_init(client_t *client);
bool client_sendmsg(client_t *client, const char *s);
bool client_input_add_block(client_t *client, const char *msg);
char *client_input_get_block(const client_t *client, int block);
int client_input_get_free_block(const client_t *client);
void client_destroy(client_t *client);

#endif /* !CLIENT_H_ */
