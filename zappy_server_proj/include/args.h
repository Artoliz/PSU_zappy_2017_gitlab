/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Args header
*/

#ifndef ARGS_H_
# define ARGS_H_

#include "zappy_server.h"

/**
* \name args_t
* \brief Represents the args passed to the server
*/
typedef struct
{
	uint16_t port; /*!< The port of the server*/
	int width; /*!< The width of the map*/
	int height; /*!< The height of the map*/
	list_t *teams; /*!< The list of teams*/
	int clients_nb; /*!< The number of players per team*/
	int freq; /*!< The refresh rate*/
} args_t;

#endif /* !ARGS_H_ */
