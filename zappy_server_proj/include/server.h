/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Server header
*/

#ifndef SERVER_H_
# define SERVER_H_

#include "zappy_server.h"
#include "network.h"
#include "client.h"

extern server_t *G_SERVER;

bool server_init(const args_t *args, server_t *server);
bool server_net_init(const args_t *args, host_t *server);
bool server_client_accept(server_t *server);
void server_client_disconnect(server_t *server, client_t *client);
void server_handle_input(server_t *server, client_t *client, const char *input);
void server_load_args(const args_t *args, server_t *server);
bool server_handle_time(server_t *server);
unsigned int server_get_slots(const server_t *server);
client_t *server_get_client(const server_t *server, const player_t *player);
bool server_game_ended(const server_t *server);
void server_cmd_forward(server_t *server, client_t *client, char * const *cmd);
void server_cmd_right(server_t *server, client_t *client, char * const *cmd);
void server_cmd_left(server_t *server, client_t *client, char * const *cmd);
void server_cmd_inventory(
	server_t *server, client_t *client, char * const *cmd);
void server_cmd_connect_nbr(
	server_t *server, client_t *client, char * const *cmd);
void server_cmd_broadcast(
	server_t *server, client_t *client, char * const *cmd);
bool server_cmd_broacast_send(char * const *cmd, int number, char *buffer);
void server_cmd_take(server_t *server, client_t *client, char * const *cmd);
void server_cmd_set(server_t *server, client_t *client, char * const *cmd);
void server_cmd_look(server_t *server, client_t *client, char * const *cmd);
void server_cmd_eject(server_t *server, client_t *client, char * const *cmd);
void server_cmd_fork(server_t *server, client_t *client, char * const *cmd);
void server_cmd_incantation(
	server_t *server, client_t *client, char * const *cmd);
void server_destroy(server_t *server);

#endif /* !SERVER_H_ */
