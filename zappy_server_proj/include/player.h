/*
** EPITECH PROJECT, 2018
** player
** File description:
** player
*/
/**
* \file
* \brief Player header
*/

#ifndef PLAYER_H_
# define PLAYER_H_

#include "zappy_server.h"
#include "team.h"
#include "map.h"
#include "list.h"

/**
* \name player_command_t
* \brief Represents a command to be stacked on the player command pile
*/
typedef struct
{
	int delay; /*!< The delay for the command*/
	char **cmd; /*!< The command and its arguments*/
} player_command_t;

/**
* \name player_t
* \brief Represents the player associated to a client
*/
typedef struct
{
	unsigned int id; /*!< The ID of the player*/
	unsigned int level; /*!< The level the player is*/
	team_t *team; /*!< The team the player belongs to*/
	map_tile_t *tile; /*!< The tile the player is on*/
	direction_t direction; /*!< The direction the player is looking at*/
	unsigned int inventory[RESOURCE_END]; /*!< The resources for a player*/
	list_t *queue; /*!< The command queue*/
} player_t;

/**
* \name player_elevation_table
* \brief Represents a member of the elevation table
*/
typedef struct
{
	unsigned int level; /*!< The level to be upgraded to*/
	unsigned int player_nb; /*!< The needed players number*/
	unsigned int resources[RESOURCE_END]; /*!< The needed resources*/
} player_elevation_table_t;

/**
* \name player_egg_t
* \brief Represents the egg a player can drop to gain a new slot
*/
typedef struct
{
	int hatch_time; /*!< The remaining time for an egg to hatch*/
	team_t *team; /*!< The team who did put the egg*/
} player_egg_t;

extern const player_elevation_table_t PLAYER_ELEVATION_TABLE[7];

bool player_init(player_t *player, map_t *map, team_t *team);
map_tile_t *player_tile_by_direction(
	const player_t *player, const direction_t *direction);
bool player_move(player_t *player, map_tile_t *tile);
player_command_t *player_push_command(
	player_t *player, int delay, char * const *cmd);
void player_pop_command(player_t *player, player_command_t *res);
int player_tile_number(
	const player_t *player, const map_coordinate_t *coordinates);
bool player_take_resource(player_t *player, resource_t resource);
bool player_drop_resource(player_t *player, resource_t resource);
void player_destroy(player_t *player);

#endif /* !PLAYER_H_ */
