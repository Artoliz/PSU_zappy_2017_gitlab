/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/

#include "server.h"

client_t *server_get_client(const server_t *server, const player_t *player)
{
	client_t *client;

	for (list_t *curr = server->clients; curr; curr = curr->next) {
		client = curr->elm;
		if (!client->greeted || client->graphical)
			continue;
		if (&client->player == player)
			return (client);
	}
	return (NULL);
}