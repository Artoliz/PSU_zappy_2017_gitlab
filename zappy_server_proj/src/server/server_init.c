/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Initializes the server and its submodules
*/

#include "server.h"
#include "map.h"

server_t *G_SERVER = NULL;

const server_commands_t SERVER_COMMANDS[] =
{
	{ "Forward", 7, server_cmd_forward },
	{ "Right", 7, server_cmd_right },
	{ "Left", 7, server_cmd_left },
	{ "Inventory", 1, server_cmd_inventory },
	{ "Connect_nbr", 7, server_cmd_connect_nbr },
	{ "Broadcast", 7, server_cmd_broadcast },
	{ "Take", 7, server_cmd_take },
	{ "Set", 7, server_cmd_set },
	{ "Look", 7, server_cmd_look },
	{ "Eject", 7, server_cmd_eject },
	{ "Fork", 42, server_cmd_fork },
	{ "Incantation", 300, server_cmd_incantation }
};

/**
* \brief Initializes the server and its submodules
* \param[in] Arguments to initializes the server from
* \param[in] The server to be initialized
* \return Fails if the initialization could not be done
*/
bool server_init(const args_t *args, server_t *server)
{
	if (!server_net_init(args, &server->data))
		return (false);
	if (!map_init(args, &server->map))
		return (false);
	G_SERVER = server;
	printf("Port : %d\n", args->port);
	return (true);
}