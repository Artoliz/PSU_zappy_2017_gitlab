/*
** EPITECH PROJECT, 2018
** serber
** File description:
** server
*/
/**
* \file
* \brief Load server settings from args
*/

#include "server.h"

/**
* \brief Load server settings from args
* \param[in] Args
* \param[in] Server
*/
void server_load_args(const args_t *args, server_t *server)
{
	server->clientsNb = args->clients_nb;
	server->frequence = args->freq;
	server->teams = args->teams;
}

