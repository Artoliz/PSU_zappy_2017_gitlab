/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** arcade
*/
/**
* \file
* \brief Handle an input coming from client
*/

#include "server.h"
#include "client.h"
#include "graphical.h"
#include "team.h"
#include "player.h"
#include "list.h"

static void handle_incantation(
	server_t *server,
	client_t *invoker,
	int idx,
	char * const *input)
{
	player_t *player;
	client_t *other;

	for (list_t *l = invoker->player.tile->content.players; l;
		l = l->next) {
		player = l->elm;
		if (list_count(player->queue) > 0) {
			client_sendmsg(invoker, "ko\nko");
			return;
		}
	}
	for (list_t *l = invoker->player.tile->content.players; l;
		l = l->next) {
		player = l->elm;
		other = server_get_client(server, player);
		client_sendmsg(other, "Elevation underway");
		player_push_command(player, SERVER_COMMANDS[idx].wait, NULL);
	}
	player_push_command(&invoker->player, SERVER_COMMANDS[idx].wait, input);
}

static void handle_client_input(
	server_t *server,
	client_t *client,
	char * const *input)
{
	const int sz = sizeof(SERVER_COMMANDS) / sizeof(server_commands_t);

	if (list_count(client->player.queue) >= 10)
		return;
	for (int it = 0; it < sz; ++it) {
		if (strcmp(SERVER_COMMANDS[it].command, input[0]))
			continue;
		if (SERVER_COMMANDS[it].fnc == server_cmd_incantation)
			handle_incantation(server, client, it, input);
		else
			player_push_command(&client->player,
				SERVER_COMMANDS[it].wait, input);
		return;
	}
	player_push_command(&client->player, 0, input);
}

static void handle_greeting_input_graphical(
	server_t *srv,
	client_t *client)
{
	team_t *team = NULL;
	const char *msg = NULL;
	char buffer[512];

	sprintf(&buffer[0], "%d %d %d\n", srv->map.coords.x,
			srv->map.coords.y, srv->frequence);
	client_sendmsg(client, &buffer[0]);
	for (list_t *teams = srv->teams; teams != NULL; teams = teams->next) {
		team = teams->elm;
		msg = "%s\t%d|";
		if (teams->next == NULL)
			msg = "%s\t%d\n";
		if (dprintf(client->data.fd, msg,
			team->name, team->number) < 0) {
			perror("Err: Msg");
			client->connected = false;
		}
	}
}

static void handle_greeting_input(
	server_t *srv,
	client_t *client,
	char * const *input)
{
	char buffer[512];
	team_t *team;

	client->graphical = (strcmp(input[0], "@GRAPHICAL") == 0);
	if (client->graphical)
		handle_greeting_input_graphical(srv, client);
	else {
		team = team_by_name(srv->teams, input[0]);
		if (!team || (srv->clientsNb + team->hatched_eggs -
			team_count_members(team)) <= 0
			|| !player_init(&client->player, &srv->map, team)) {
			client_sendmsg(client, strcpy(&buffer[0], "ko"));
			return;
		}
		sprintf(&buffer[0], "%d\n%d %d",
			srv->clientsNb - team_count_members(team),
			srv->map.coords.x, srv->map.coords.y);
		client_sendmsg(client, &buffer[0]);
	}
	client->greeted = true;
}

/**
* \brief Handle input coming from client
* \param[in] The server
* \param[in] The client sending the input
* \param[in] The input sent by the client
*/
void server_handle_input(server_t *server, client_t *client, const char *input)
{
	char **wt = utils_strsplit(input, " ", 0);

	if (!wt)
		return;
	if (!client->greeted)
		handle_greeting_input(server, client, wt);
	else {
		if (client->graphical)
			graphical_send_map(server, client);
		else
			handle_client_input(server, client, wt);
	}
	utils_wt_destroy(wt);
}