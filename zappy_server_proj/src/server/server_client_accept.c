/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Accept Client Connection
*/

#include "server.h"
#include "client.h"

static bool set_client_poll_status(
	server_t *server,
	client_t *client,
	int action)
{
	struct epoll_event ev;

	ev.events = EPOLLIN;
	ev.data.ptr = client;
	if (epoll_ctl(server->epfd, action, client->data.fd, &ev) == (-1))
		return (utils_perror_ret("epoll_ctl"));
	return (true);
}

static bool clean_on_error(const char *s, void *to_free)
{
	free(to_free);
	if (s)
		fprintf(stderr, "Error: %s\n", s);
	return (false);
}

/**
* \brief Accept client connection
* \param[in] Server to accept connection from
*/
bool server_client_accept(server_t *server)
{
	client_t *client = calloc(1, sizeof(client_t));
	socklen_t len = sizeof(struct sockaddr_in);

	if (!client)
		return (utils_perror_ret("malloc"));
	client->data.fd = accept(
		server->data.fd, (struct sockaddr *) &client->data.addr, &len);
	if (client->data.fd == (-1))
		return (clean_on_error("accept failed", client));
	if (!set_client_poll_status(server, client, EPOLL_CTL_ADD))
		return (clean_on_error(NULL, client));
	if (!list_push(&server->clients, client)) {
		clean_on_error("Could not push client\n", client);
		set_client_poll_status(server, client, EPOLL_CTL_DEL);
		return (false);
	}
	client_init(client);
	client_sendmsg(client, "WELCOME");
	return (true);
}