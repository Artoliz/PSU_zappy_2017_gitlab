/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Init Server Structure
*/

#include "server.h"

static void set_server_net_infos(host_t *server)
{
	struct sockaddr_in sockaddr;
	socklen_t sz = sizeof(struct sockaddr_in);

	if (getsockname(server->fd, (struct sockaddr *) &sockaddr,
		&sz) == (-1))
		return;
	server->port = ntohs(sockaddr.sin_port);
}

static bool close_and_perrorret(int sock, const char *s)
{
	close(sock);
	return (utils_perror_ret(s));
}

/**
* \brief Initializes server making it ready to accept connections
* \param[in] Program Arguments
* \param[out] Server to be initialised
* \return Returns if the function failed
*/
bool server_net_init(const args_t *args, host_t *server)
{
	server->fd = socket(AF_INET, SOCK_STREAM, 0);
	if (server->fd == (-1))
		return (utils_perror_ret("socket"));
	if (setsockopt(server->fd, SOL_SOCKET, SO_REUSEADDR,
		&(int) { 1 }, sizeof(int)))
		perror("setsockopt");
	memset(&server->addr, 0, sizeof(struct sockaddr));
	server->addr.sin_family = AF_INET;
	server->addr.sin_addr.s_addr = INADDR_ANY;
	server->addr.sin_port = htons(args->port);
	if (bind(server->fd, (struct sockaddr *) &server->addr,
		sizeof(struct sockaddr_in)) == (-1))
		return (close_and_perrorret(server->fd, "bind"));
	if (listen(server->fd, 100) == (-1))
		return (close_and_perrorret(server->fd, "listen"));
	set_server_net_infos(server);
	return (true);
}