/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Checks whether a team won or not
*/

#include "server.h"

static bool members_won(const team_t *team)
{
	player_t *player;
	int valid_players = 0;

	for (list_t *curr = team->members; curr; curr = curr->next) {
		player = curr->elm;
		if (player->level >= 8)
			++valid_players;
		if (valid_players >= 6)
			return (true);
	}
	return (false);
}

/**
* \brief Checks whether a team won or not
* \param[in] The server to check the team from
* \return Returns true if found, false otherwise
*/
bool server_game_ended(const server_t *server)
{
	team_t *team;

	for (list_t *curr = server->teams; curr; curr = curr->next) {
		team = curr->elm;
		if (members_won(team)) {
			printf("Game Ended: Team %s won !\n", team->name);
			return (true);
		}
	}
	return (false);
}