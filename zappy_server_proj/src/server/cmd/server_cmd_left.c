/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Handle LEFT command
*/

#include "server.h"

/**
* \brief Handle LEFT command
* \param[in] The server
* \param[in] The client sending the command
* \param[in] The client command arguments
*/
void server_cmd_left(
	UNUSED server_t *server,
	client_t *client,
	UNUSED char * const *cmd)
{
	switch (client->player.direction) {
	case DIRECTION_NORTH:
		client->player.direction = DIRECTION_WEST;
		break;
	case DIRECTION_WEST:
		client->player.direction = DIRECTION_SOUTH;
		break;
	case DIRECTION_SOUTH:
		client->player.direction = DIRECTION_EAST;
		break;
	case DIRECTION_EAST:
		client->player.direction = DIRECTION_NORTH;
		break;
	}
	client_sendmsg(client, "ok");
}