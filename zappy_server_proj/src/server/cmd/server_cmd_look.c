/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Handle LOOK command
*/

#include "server.h"

static void concat_buffer(char (*buffer)[8192], const char *s)
{
	if ((strlen(buffer[0]) + strlen(s)) < sizeof(*buffer) - 2)
		strcat(buffer[0], s);
}

static void concat_resource(
	char (*buffer)[8192],
	resource_t resource,
	bool *first,
	const map_tile_t *tile)
{
	for (unsigned int it = 0; it < tile->content.resources[resource];
		++it) {
		if (!*first)
			concat_buffer(buffer, " ");
		else
			*first = false;
		concat_buffer(buffer, map_name_by_resource(resource));
	}
}

static void concat_tile(char (*buffer)[8192], const map_tile_t *tile)
{
	bool first = true;

	for (list_t *curr = tile->content.players; curr; curr = curr->next) {
		if (first) {
			concat_buffer(buffer, "player");
			first = false;
		}
		else
			concat_buffer(buffer, " player");
	}
	for (resource_t resource = 0; resource != RESOURCE_END; ++resource)
		concat_resource(buffer, resource, &first, tile);
}

static void concat_tiles_levels(
	char (*buffer)[8192],
	unsigned int level,
	const player_t *player)
{
	const map_tile_t *tile = player->tile;

	for (unsigned int it = 0; it < level; ++it) {
		tile = map_tile_by_relative_direction(
			tile, player->direction, DIRECTION_NORTH);
	}
	for (unsigned int it = 0; it < level; ++it) {
		tile = map_tile_by_relative_direction(
			tile, player->direction, DIRECTION_WEST);
	}
	for (unsigned int it = 0; it < level * 2 + 1; ++it) {
		if (level > 0)
			concat_buffer(buffer, ",");
		concat_tile(buffer, tile);
		tile = map_tile_by_relative_direction(
			tile, player->direction, DIRECTION_EAST);
	}
}

/**
* \brief Handle LOOK command
* \param[in] The server
* \param[in] The client sending the command
* \param[in] The client command arguments
*/
void server_cmd_look(
	UNUSED server_t *server,
	client_t *client,
	UNUSED char * const *cmd)
{
	char buffer[8192] = { 0 };
	size_t len;

	buffer[0] = '[';
	for (unsigned int level = 0; level <= client->player.level; ++level)
		concat_tiles_levels(&buffer, level, &client->player);
	len = strlen(&buffer[0]);
	buffer[len] = ']';
	buffer[len + 1] = 0;
	client_sendmsg(client, &buffer[0]);
}