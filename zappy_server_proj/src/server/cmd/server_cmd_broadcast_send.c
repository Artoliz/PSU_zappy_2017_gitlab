/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Send the message to players.
*/
/**
* \file
* \brief Fill the buffer with the message.
*/

#include "client.h"
#include "player.h"
#include "network.h"
#include "map.h"

/**
* \brief Fill the buffer with the message.
* \param[in] cmd The command array.
* \param[in] number Tile numer around the player.
* \param[out] buffer The buffer to be filled.
* \return true if everything went well, false otherwise.
*/
bool server_cmd_broacast_send(char * const *cmd, int number, char *buffer)
{
	int written = 0;

	written = snprintf(&buffer[0], 1023, "message %d, %s", number, cmd[1]);
	for (int i = 2; cmd[i] != NULL; i++) {
		written += snprintf(&buffer[written],
			1023 - written, " %s", cmd[i]);
	}
	return (true);
}