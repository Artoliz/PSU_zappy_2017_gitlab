/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Handle INVENTORY command
*/

#include "server.h"

/**
* \brief Handle INVENTORY command
* \param[in] The server
* \param[in] The client sending the command
* \param[in] The client command arguments
*/
void server_cmd_inventory(
	UNUSED server_t *server,
	client_t *client,
	UNUSED char * const *cmd)
{
	char buffer[1024];
	player_t *player = &client->player;

	sprintf(&buffer[0], "[food %u, linemate %u, deraumere %u, sibur %u, "
		"mendiane %u, phiras %u, thystame %u]",
		player->inventory[RESOURCE_FOOD] / 126,
		player->inventory[RESOURCE_LINEMATE],
		player->inventory[RESOURCE_DERAUMERE],
		player->inventory[RESOURCE_SIBUR],
		player->inventory[RESOURCE_MENDIANE],
		player->inventory[RESOURCE_PHIRAS],
		player->inventory[RESOURCE_THYSTAME]);
	client_sendmsg(client, &buffer[0]);
}