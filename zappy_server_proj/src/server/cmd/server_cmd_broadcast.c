/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Broadcast command.
*/
/**
* \file
* \brief Handle CONNECT_NBR command
*/

#include "client.h"
#include "player.h"
#include "network.h"
#include "map.h"
#include "math.h"
#include "server.h"

static int find_min_value(float *a, int size)
{
	float save = a[0];
	int res = 0;

	for (int i = 1; i < size; i++) {
		if (a[i] < save) {
			res = i;
			save = a[i];
		}
	}
	return (res);
}

static map_coordinate_t *find_best_translation(
	const map_coordinate_t *emitter_coord,
	const map_coordinate_t *receiver_coord,
	const map_coordinate_t *size)
{
	float distances[9];
	map_coordinate_t translate[9] = {{-size->x, -size->y}, {0, -size->y},
		{size->x, -size->y}, {-size->x, 0}, {0, 0}, {size->x, 0},
		{-size->x, size->y}, {0, size->y}, {size->x, size->y}};
	map_coordinate_t *res = NULL;
	int best = 0;

	for (int i = 0; i < 9; i++)
		distances[i] = sqrtf(powf(emitter_coord->x -
		(receiver_coord->x + translate[i].x), 2) + powf(
		emitter_coord->y - (receiver_coord->y + translate[i].y), 2));
	best = find_min_value(distances, 9);
	res = malloc(sizeof(map_coordinate_t));
	if (res == NULL)
		return (NULL);
	res->x = translate[best].x;
	res->y = translate[best].y;
	return (res);
}

static void find_receiving_tile(
	const map_coordinate_t *emitter_coord,
	const map_coordinate_t *receiver_coord,
	const map_coordinate_t *translation,
	map_coordinate_t *surround)
{
	map_coordinate_t translated_receiver = {
		receiver_coord->x + translation->x,
		receiver_coord->y + translation->y};
	float distances[9];
	map_coordinate_t around[9] = {{-1, -1}, {0, -1}, {1, -1}, {-1, 0},
				{0, 0}, {1, 0}, {-1, 1}, {0, 1}, {1, 1}};
	int best = 0;

	for (int i = 0; i < 9; i++)
		distances[i] = sqrtf(powf(emitter_coord->x -
		(translated_receiver.x + around[i].x), 2) + powf(
		emitter_coord->y - (translated_receiver.y + around[i].y), 2));
	best = find_min_value(distances, 9);
	surround->x = receiver_coord->x + around[best].x;
	surround->y = receiver_coord->y + around[best].y;
}

static bool broadcast_to_client(
	const client_t *emitter,
	const client_t *receiver,
	const map_coordinate_t *size,
	map_coordinate_t *surround)
{
	map_coordinate_t emitter_coord = {emitter->player.tile->coord.x,
					emitter->player.tile->coord.y};
	map_coordinate_t receiver_coord = {receiver->player.tile->coord.x,
					receiver->player.tile->coord.y};
	map_coordinate_t *translation = find_best_translation(&emitter_coord,
		&receiver_coord, size);

	if (translation == NULL)
		return (false);
	find_receiving_tile(&emitter_coord, &receiver_coord,
		translation, surround);
	free(translation);
	return (true);
}

/**
* \brief Handle BROADCAST command
* \param[in] The server
* \param[in] The client sending the command
* \param[in] The client command arguments
*/
void server_cmd_broadcast(
	server_t *srv,
	client_t *client,
	char * const *cmd)
{
	client_t *curr;
	map_coordinate_t tile;
	char buffer[1024] = { 0 };

	if (!cmd[1]) {
		client_sendmsg(client, "ko");
		return;
	}
	for (list_t *cli = srv->clients; cli; cli = cli->next) {
		curr = cli->elm;
		if (!curr->greeted || curr->graphical || client == curr)
			continue;
		broadcast_to_client(client, cli->elm, &srv->map.coords, &tile);
		tile.x = MOD(tile.x, srv->map.coords.x);
		tile.y = MOD(tile.y, srv->map.coords.y);
		server_cmd_broacast_send(cmd,
			player_tile_number(&curr->player, &tile), buffer);
		client_sendmsg(cli->elm, &buffer[0]);
	}
	client_sendmsg(client, "ok");
}