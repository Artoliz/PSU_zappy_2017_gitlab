/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Handle EJECT command
*/

#include "server.h"

/**
* \brief Handle EJECT command
* \param[in] The server
* \param[in] The client sending the command
* \param[in] The client command arguments
*/
void server_cmd_eject(
	server_t *server,
	client_t *client,
	UNUSED char * const *cmd)
{
	client_t *curr_client;

	for (list_t *curr = server->clients; curr; curr = curr->next) {
		curr_client = curr->elm;
		if (!curr_client->greeted || curr_client->graphical ||
			curr_client->player.tile != client->player.tile ||
			curr_client == client)
			continue;
		player_move(&curr_client->player,
			player_tile_by_direction(&curr_client->player, NULL));
		client_sendmsg(curr_client, "eject: 5");
	}
	client_sendmsg(client, "ok");
}