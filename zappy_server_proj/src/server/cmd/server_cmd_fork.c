/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Fork command.
*/
/**
* \file
* \brief Fork command.
*/

#include "client.h"
#include "player.h"
#include "network.h"
#include "map.h"

/**
* \brief Fork command allows a player to reproduce himself.
* \param[in] server The server's informations.
* \param[in,out] client The client's informations.
* \param[in] cmd The player typed command.
*/
void server_cmd_fork(
	UNUSED server_t *server,
	client_t *client,
	UNUSED char * const *cmd)
{
	player_egg_t *egg = malloc(sizeof(player_egg_t));

	if (egg == NULL)
		return;
	egg->hatch_time = 600;
	egg->team = client->player.team;
	if (client->player.tile == NULL ||
		!list_push(&client->player.tile->content.eggs, egg))
		client_sendmsg(client, "ko");
	client_sendmsg(client, "ok");
}