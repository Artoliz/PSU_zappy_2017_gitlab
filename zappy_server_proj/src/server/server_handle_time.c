/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Manage player command queue.
*/
/**
* \file
* \brief Hanlde time related actions.
*/

#include "network.h"
#include "server.h"

static void execute_command(
	server_t *server,
	client_t *client,
	char * const *input)
{
	const int sz = sizeof(SERVER_COMMANDS) / sizeof(server_commands_t);

	for (int it = 0; it < sz; ++it) {
		if (strcmp(SERVER_COMMANDS[it].command, input[0]) == 0) {
			SERVER_COMMANDS[it].fnc(server, client, input);
			return;
		}
	}
	client_sendmsg(client, "ko");
}

static bool decrement_life(
	server_t *server,
	client_t *client)
{
	player_t *player = &client->player;

	if (client->graphical)
		return (true);
	player->inventory[RESOURCE_FOOD] -= 1;
	if (player->inventory[RESOURCE_FOOD] <= 0) {
		client_sendmsg(client, "dead");
		server_client_disconnect(server, client);
		return (false);
	}
	return (true);
}

static bool decrement_queue(
	server_t *server,
	client_t *client)
{
	player_t *player = &client->player;
	player_command_t *command;
	list_t *curr = player->queue;

	if (curr == NULL)
		return (true);
	command = curr->elm;
	--command->delay;
	if (command->delay <= 0) {
		if (command->cmd && command->cmd[0])
			execute_command(server, client, command->cmd);
		player_pop_command(player, command);
	}
	return (true);
}

static bool decrement_hatching(map_tile_t *tile)
{
	list_t *eggs = NULL;

	if (tile == NULL || tile->content.eggs == NULL)
		return (true);
	eggs = tile->content.eggs;
	for (player_egg_t *egg = eggs->elm; eggs != NULL; ) {
		egg->hatch_time -= 1;
		if (egg->hatch_time <= 0) {
			eggs = eggs->next;
			list_pop_by_elm(&tile->content.eggs, egg);
			++egg->team->hatched_eggs;
			free(egg);
		} else
			eggs = eggs->next;
	}
	return (true);
}

/**
* \brief Handle all off the time decrementation and actions for the server.
* \param[in,out] server The server informations.
* \return true if everything went well, false otherwise.
*/
bool server_handle_time(server_t *server)
{
	list_t *tmp = server->clients;
	list_t *next;
	map_tile_t *tiles = server->map.tiles;

	while (tmp) {
		next = tmp->next;
		if (decrement_life(server, tmp->elm))
			decrement_queue(server, tmp->elm);
		tmp = next;
	}
	for (int i = 0; i < server->map.coords.y; i++) {
		for (int j = 0; j < server->map.coords.x; j++) {
			tiles = tiles->east;
			decrement_hatching(tiles);
		}
		tiles = tiles->south;
	}
	return (true);
}