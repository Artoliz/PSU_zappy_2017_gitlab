/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Disconnect client
*/

#include "server.h"

void server_client_disconnect(server_t *server, client_t *client)
{
	if (epoll_ctl(
		server->epfd, EPOLL_CTL_DEL, client->data.fd, NULL) == (-1))
		perror("epoll_ctl (del)");
	list_pop_by_elm(&server->clients, client);
	client_destroy(client);
	free(client);
}