/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Init command queue node.
*/

#include "player.h"

player_command_t *player_push_command(
	player_t *player,
	int delay,
	char * const *cmd)
{
	player_command_t *res = malloc(sizeof(player_command_t));

	if (res == NULL)
		return (NULL);
	res->cmd = utils_wt_copy(cmd);
	if (!res->cmd)
		return (NULL);
	res->delay = delay;
	if (!list_push(&player->queue, res)) {
		utils_wt_destroy(res->cmd);
		free(res);
		return (NULL);
	}
	return (res);
}