/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Move a player according to a direction and the way it is looking
*/

#include "player.h"

/**
* \brief Move a player according to a direction and the way it is looking
* \param[in] The player to move
* \param[in] The direction to move the player to
*/
bool player_move(player_t *player, map_tile_t *tile)
{
	if (!list_push(&tile->content.players, player))
		return (false);
	list_pop_by_elm(&player->tile->content.players, player);
	player->tile = tile;
	return (true);
}