/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Take a resource on the current tile
*/

#include "player.h"

/**
* \brief Take a resource on the current tile
* \param[in] The player getting the resource
* \param[in] The resource to take
* \return Fails if the resource is not on the tile
*/
bool player_take_resource(player_t *player, resource_t resource)
{
	if (player->tile->content.resources[resource] <= 0)
		return (false);
	--player->tile->content.resources[resource];
	if (resource == RESOURCE_FOOD)
		player->inventory[resource] += 126;
	else
		++player->inventory[resource];
	return (true);
}