/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Retrieves a team according to its name
*/

#include "team.h"

/**
* \brief Retrieves a team according to its name
* \param[in] The list of teams
* \param[in] Name of the team to be found
* \return Returns a team if found, other wise returns NULL
*/
team_t *team_by_name(const list_t *teams, const char *name)
{
	team_t *team = NULL;

	for (const list_t *curr = teams; curr; curr = curr->next) {
		if (strcmp(((team_t *) curr->elm)->name, name) == 0)
		{
			team = curr->elm;
			break;
		}
	}
	return (team);
}