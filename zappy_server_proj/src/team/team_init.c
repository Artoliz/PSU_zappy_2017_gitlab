/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Initialize team structure
*/

#include "team.h"

/**
* \brief Initialize team structure
* \param[in] The team to be initialized
*/
bool team_init(team_t *team, const char *name)
{
	static int number = 1;

	for (size_t it = 0; name[it]; ++it) {
		if (!isalpha(name[it]) && !isdigit(name[it]))
			return (utils_error_ret("team needs an alphanum name"));
	}
	team->name = strdup(name);
	if (!team->name)
		return (utils_perror_ret("team init: name"));
	team->members = NULL;
	team->number = number++;
	team->hatched_eggs = 0;
	return (true);
}