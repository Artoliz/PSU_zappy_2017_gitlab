/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Destroy the client
*/

#include "client.h"

/**
* \brief Destroy the client
* \param[in] The client to destroy
*/
void client_destroy(client_t *client)
{
	if (client->greeted && !client->graphical)
		player_destroy(&client->player);
	close(client->data.fd);
}