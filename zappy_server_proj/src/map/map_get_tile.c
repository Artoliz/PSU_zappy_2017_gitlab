/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Get a tile at given coordinates
*/

#include "map.h"

/**
* \brief Get a tile at given coordinates
* \param[in] The map to get the tile from
* \param[in] X Coordinate
* \param[in] Y Coordinate
* \return The tile at the given coordinates or NULL if not found
*/
map_tile_t *map_get_tile(const map_t *map, const map_coordinate_t *coords)
{
	map_tile_t *tile = map->tiles;

	if ((coords->x < 0 || coords->x > map->coords.x) ||
		(coords->y < 0 || coords->y > map->coords.y))
		return (NULL);
	for (int it = 0; it < coords->x; ++it)
		tile = tile->east;
	for (int it = 0; it < coords->y; ++it)
		tile = tile->south;
	return (tile);
}
