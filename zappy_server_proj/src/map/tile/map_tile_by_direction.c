/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Get next tile according to direction
*/

#include "map.h"

/**
* \brief Get next tile according to direction
* \param[in] The tile of reference
* \param[in] The direction to look for
* \return The tile given according to the direction
*/
map_tile_t *map_tile_by_direction(const map_tile_t *tile, direction_t direction)
{
	map_tile_t *ret = NULL;

	switch (direction) {
	case DIRECTION_NORTH:
		ret = tile->north;
		break;
	case DIRECTION_WEST:
		ret = tile->west;
		break;
	case DIRECTION_EAST:
		ret = tile->east;
		break;
	case DIRECTION_SOUTH:
		ret = tile->south;
		break;
	}
	return (ret);
}
