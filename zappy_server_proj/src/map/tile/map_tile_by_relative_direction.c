/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Retrieve the neighbour tile according to a relative direction
*/

#include "map.h"

/**
* \brief Retrieve the neighbour tile according to a relative direction
* \param[in] The tile to start from
* \param[in] The direction of the tile
* \param[in] The relative direction depending of the tile direction
* \return The tile found
*/
map_tile_t *map_tile_by_relative_direction(
	const map_tile_t *tile,
	direction_t absolute,
	direction_t relative)
{
	const int sz = sizeof(MAP_DIRECTION_TABLE) / sizeof(direction_table_t);

	for (int it = 0; it < sz; ++it) {
		if (MAP_DIRECTION_TABLE[it].absolute == absolute &&
			MAP_DIRECTION_TABLE[it].relative == relative)
			return (map_tile_by_direction(
				tile, MAP_DIRECTION_TABLE[it].result));
	}
	return (NULL);
}