/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Init tiles with default resources in it
*/

#include "map.h"

static void map_init_egg(map_t *map)
{
	map_tile_t *tiles = map->tiles;

	for (int i = 0; i < map->coords.y; i++) {
		for (int j = 0; j < map->coords.x; j++) {
			tiles->content.eggs = NULL;
			tiles = tiles->east;
		}
		tiles = tiles->south;
	}
}

/**
* \brief Init tiles with default resources in it
* \param[in] The map to init the tiles of
*/
void map_init_tiles(map_t *map, UNUSED int max_players)
{
	unsigned int max_food = map->coords.x * map->coords.y * 3;
	unsigned int max_resources = map->coords.x * map->coords.y * 1;

	for (unsigned int it = 0; it < max_food; ++it)
		++map_get_tile(map, &(map_coordinate_t) { rand() %
			map->coords.x, rand() % map->coords.y }
			)->content.resources[RESOURCE_FOOD];
	for (resource_t resource = 0; resource < RESOURCE_END; ++resource) {
		for (unsigned int it = 0; it < max_resources; ++it)
			++map_get_tile(map, &(map_coordinate_t) { rand() %
				map->coords.x, rand() % map->coords.y }
				)->content.resources[resource];
	}
	map_init_egg(map);
}