/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Destroys map content
*/

#include "map.h"

/**
* \brief Destroys map content
* \param[in] The map to be destroyed
*/
void map_destroy(map_t *map)
{
	map_tile_t *next_head = map->tiles;
	map_tile_t *curr;
	map_tile_t *curr_next;

	for (int it = 0; it < map->coords.y; ++it) {
		curr = next_head;
		next_head = next_head->south;
		for (int ij = 0; ij < map->coords.x; ++ij) {
			curr_next = curr->east;
			list_destroy(
				&curr->content.players, LIST_FREE_NOT, NULL);
			list_destroy(
				&curr->content.eggs, LIST_FREE_PTR, NULL);
			free(curr);
			curr = curr_next;
		}
	}
}