/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Retrieve the name of a resource according to its type
*/

#include "map.h"

/**
* \brief Retrieve the name of a resource according to its type
* \param[in] The resource to get the name of
* \return The name if found, otherwise "unknown"
*/
const char *map_name_by_resource(resource_t resource)
{
	const int sz =
		sizeof(MAP_RESOURCE_NAME_TABLE) / sizeof(resource_name_table_t);

	for (int it = 0; it < sz; ++it) {
		if (MAP_RESOURCE_NAME_TABLE[it].resource == resource)
			return (MAP_RESOURCE_NAME_TABLE[it].name);
	}
	return ("unknown");
}