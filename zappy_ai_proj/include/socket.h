/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Define Socket's Header
*/
/**
* \file
* \brief Define Socket's Header
*/

#ifndef SOCKET_H_
# define SOCKET_H_

typedef struct s_socket
{
	int fd;
	int port;
	char *addr;
} t_socket;

#endif /* !SOCKET_H_ */