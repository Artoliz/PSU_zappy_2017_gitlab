/*
** EPITECH PROJECT, 2018
** PSU_zappy_2017
** File description:
** server fd save on client side c code
*/
/**
* \file
* \brief This file contains the function setter-getter of the server fd
*/

#include "ai.h"
#include <unistd.h>

/**
* \brief save the fd
* \param[in] sfd instantiate at connection
* \return the saved fd
*/
int saved_fd(int sfd)
{
	static int fd = -1;

	if (sfd == -2 || sfd > 2) {
		if (fd > 2)
			close(fd);
		fd = sfd;
	}
	return (fd);
}