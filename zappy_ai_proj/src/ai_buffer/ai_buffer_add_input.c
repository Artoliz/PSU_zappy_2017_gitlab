/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** Functions relative to the buffer_t struct
*/
/**
* \file
* \brief Contains the function adding a string on the responses command list
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include "ai.h"
#include "utils.h"
#include "buffer.h"

static int find_end_low(buffer_t *buf, int tmp)
{
	for (; tmp < buf->w; tmp++) {
		if (buf->buf[tmp] == '\n')
			return (tmp);
	}
	return (-1);
}

static int find_end_high(buffer_t *buf, int tmp)
{
	for (; tmp < buf->size; tmp++)
		if (buf->buf[tmp] == '\n')
			return (tmp);
	for (tmp = 0; tmp < buf->w; tmp++)
		if (buf->buf[tmp] == '\n')
			return (tmp);
	return (-1);
}

static int find_end(buffer_t *buf)
{
	int tmp = buf->r;

	if (tmp < buf->w)
		return (find_end_low(buf, tmp));
	else if (tmp > buf->w)
		return (find_end_high(buf, tmp));
	return (-1);
}

static int read_buffer(buffer_t *buf)
{
	char line[RECV_SIZE];
	int ret = 0;
	int tmp = 0;

	for (int end = find_end(buf); end != -1; end = find_end(buf)) {
		memset(line, 0, RECV_SIZE);
		if (end > buf->r)
			strncpy(line, buf->buf + buf->r, end - buf->r);
		else if (end != buf->r) {
			strncpy(line, buf->buf + buf->r, buf->size - buf->r);
			strncpy(line + (buf->size - buf->r), buf->buf, end);
		}
		buf->r = end + 1 >= RECV_SIZE ? 0 : end + 1;
		tmp = ai_cmdlist_add_element(line);
		if (tmp == -1)
			return (-1);
		ret += tmp;
	}
	return (ret);
}

/**
* \brief add an input to the client buffer
* \param[in,out] pointer to structure buffer used. Must be allocated and
** initialised.
* \param[in] input to be add
* \param[in] size of the input to be add
* \return nunber of full responses received. -1 on error.
*/
int ai_buffer_add_input(buffer_t *buf, char *tmp, int r)
{
	int i = 0;
	int ret = 0;

	for (int a = buf->w; a < buf->size && i < r; a++, ++i) {
		buf->buf[a] = tmp[i];
		buf->w += 1;
	}
	if (i < r) {
		for (buf->w = 0; i < r && i < buf->size; ++i) {
			buf->buf[buf->w] = tmp[i];
			buf->w += 1;
		}
	}
	buf->w = buf->w < RECV_SIZE ? buf->w : 0;
	if (i == buf->size && i < r) {
		ret = ai_buffer_add_input(buf, tmp + i, r - i);
		buf->r = (buf->w < RECV_SIZE - 1 ? buf->w + 1 : 0);
		return (ret);
	}
	return (read_buffer(buf));
}