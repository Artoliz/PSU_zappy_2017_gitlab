#!/usr/bin/python3.6

ORDER = ["food", "linemate", "deraumere", "sibur", "mendiane", "phiras",
	"thystame"]

MAX_LVL = 8

class Tools:
	"""Tools used by Trantorians"""

	@classmethod
	def _cleanLook(cls, answer):
		"""Clean '[' and ']' from 'Look' command

		Args:
			answer: The answer who need to be modified.

		Return:
			Return the answer corresponding.

		"""

		convert = {"[ ": "", " ]": "", "[": "", "]": ""}
		for to_convert, convert_in in convert.items():
			answer = answer.replace(to_convert, convert_in)
		return answer

	@classmethod
	def _cleanInventory(cls, answer):
		"""Convert answer as an int array, sorting if necessary

		Args:
			answer: The answer who need to be modified.

		Return:
			Return the int answer corresponding to food and stones.

		"""

		tab = []
		convert = {"[ ": "", " ]": "", "[": "", "]": ""}
		for to_convert, convert_in in convert.items():
			answer = answer.replace(to_convert, convert_in)
		for item in answer.split(","):
			item = item.split(" ")
			if item[0] == '':
				item.pop(0)
			i = ORDER.index(item[0])
			tab.insert(i, int(item[1]))
		return tab

	@classmethod
	def _convertTileToSeq(cls, tile):
		"""Convert a tile to a sequence of movement to reach it

		Args:
			tile: Tile to reach

		Return:
			rank: Item's line where we founded it on look
			dist: Distance from middle where the item resides
		"""

		rank = -1
		if tile == 0:
			return 0, 0
		for i in range(0, MAX_LVL):
			middle = i + (i * i)
			if tile <= middle + i and tile >= middle - i:
				rank = i
				break
		dist = tile - middle
		return rank, dist
