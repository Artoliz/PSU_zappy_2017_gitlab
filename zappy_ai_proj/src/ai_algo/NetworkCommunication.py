#!/usr/bin/python3.6

from ctypes import cdll
from ctypes import c_char_p

SERVER_ERR = "Couldn't connect to the server."
SHARED_LIB_ERR = "Couldn't load a function from the shared library"

class LdlTools:
	"""LdlTools class.

	Tools class used to manage the download of shared library.

	Raise:
		Raise an EnvironmentError if the shared library
		can't be loaded.

	Attributes:
		_ldl: an instance of the open shared library.
	"""

	def __init__(self, nameSharedLibrary):
		"""Open the shared library"""

		if self.getLdlByName(nameSharedLibrary) == True:
			func = self._ldl.ai_cmdlist_pop_element
			func.restype = c_char_p
		else:
			raise EnvironmentError

	def getLdlByName(self, nameSharedLibrary):
		"""Download the shared library and store it.

		Args:
			nameSharedLibrary: nameSharedLibrary of the library
			to download.
		"""

		try:
			self._ldl = cdll.LoadLibrary(nameSharedLibrary)
		except:
			print("Can't load the connection library.")
			return False
		return True

	def getLdl(self):
		"""Getter to the ldl attributes.

		Returns:
			ldl attribute
		"""

		return self._ldl

class NetworkCommunication:
	"""Network communication with the server.

	This class is used to communicate with the server thanks to the network
	layer, which has been made in C find in the given library name.

	Attributes:
		_ldlTools: tool class to use the shared library.
	"""

	def __init__(self, nameSharedLibrary):
		"""Constructor.

		Raise:
			Raise an Exception if the object ldlTools failed
			to be created

		Args:
			nameSharedLibrary: name of the shared library.
		"""

		try:
			self._ldlTools = LdlTools(nameSharedLibrary)
			self._value = 0
		except:
			raise Exception("Can't instantiate ldlTools")

	def connectToServer(self, **kwargs):
		"""Connect to a server with the given parameters.

		Raise:
			Raise ConnectionError describing the failure,
			if we can't connect to the server,
			or the arguments aren't good.

		Args:
			argc: number of arguments.
			argv: tab of arguments.
		"""

		if len(kwargs) != 2:
			raise ConnectionError("Too much/Not enough arguments \
passed to connectTo() to connect to the server.")
		argc = kwargs.get("arg1")
		argv = kwargs.get("arg2")
		Args = c_char_p * argc
		args = Args(*[c_char_p(arg.encode("utf-8")) for arg in argv])
		try:
			if self._ldlTools.getLdl().main_connection(argc, args)\
			 != 0:
				raise ConnectionError(SERVER_ERR)
		except:
			raise ConnectionError(SERVER_ERR)

	def isAvailable(self):
		"""Return a mask describing if we can get data from the server.

		Raise:
			Raise a ConnectionError if the function isn't find.

		Returns:
			Return a mask describing the operation
		"""

		try:
			return self._ldlTools.getLdl().is_available()
		except:
			raise ConnectionError(SHARED_LIB_ERR)

	def sendCommand(self, **kwargs):
		"""Send the given command to the server

		Args:
			command: command to send to the server.

		Raise:
			Raise an exception ConnectionError if we can't find
			the function or the arguments aren't good.

		Returns:
			Return 0 if it succeed or -1 on failure.
		"""

		try:
			command = kwargs.get("arg1")
			cmd = command.encode("utf-8")
			ret = self._ldlTools.getLdl().send_to_serv(cmd)
		except:
			raise ConnectionError(SHARED_LIB_ERR)
		if ret < 0:
			raise ConnectionError("Server Error.")
		return ret

	def getCommand(self, **kwargs):
		"""Get the first command on the stack of the server.

		Raise:
			Raise a ConnectionError if we can't load the function.

		Returns:
			Return an empty command or the first command added to
			the stack
		"""

		try:
			if self._value == 0:
				self._value = self.isAvailable()
				if self._value == -1:
					raise ConnectionError("Server down...")
			self._value -= 1
			ret = self._ldlTools.getLdl().ai_cmdlist_pop_element()
			return ret.decode("utf-8")
		except:
			raise ConnectionError(SHARED_LIB_ERR)

	def cleanRessources(self):
		"""Clean the ressources before exiting

		Raise:
			Raise a ConnectionError if we can't load the function.

		Return:
			Return 0
		"""

		try:
			return self._ldlTools.getLdl().clean_ressources()
		except:
			raise ConnectionError(SHARED_LIB_ERR)