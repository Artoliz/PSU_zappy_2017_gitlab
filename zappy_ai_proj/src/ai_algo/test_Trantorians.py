#!/usr/bin/python3.6

from time import time
from Trantorians import Trantorians

def testTrantoriansInit():
	tantor = Trantorians()
	assert tantor._broadcastString == []
	assert tantor._awaitedPlayer == 0
	assert tantor._toSearch == ["food"]
	assert tantor._level == 1
	assert tantor._unexpected == False
	assert tantor._foodNb == 0
	assert tantor._nbPlayersSameLevel == 0
	assert tantor._isWaiting == False
	assert tantor._timeCmdTeamLevel == 0
	assert tantor._onlyFood == False
	assert tantor._inIncantation == False
	assert tantor._playerForIncantation == []
	assert tantor._isManager == False
	assert tantor._incantationLoop == 0
	assert tantor._sendArrived == False

def testCommand():
	tantor = Trantorians()
	try:
		tantor._command("FakeCmd")
	except SystemExit as SE:
		assert SE.code == 84

def testIdentification():
	tantor = Trantorians()
	try:
		tantor._identification("4")
	except SystemExit as SE:
		assert SE.code == 84

def testConnectMe():
	tantor = Trantorians()
	try:
		tantor.connectMe(5, ["-n", "yolo", "-p", "4242"])
	except SystemExit as SE:
		assert SE.code == 84

def testConnectNbr():
	tantor = Trantorians()
	tantor._connectNbr("0")
	assert tantor._responseQueue[0] == "Fork"
	tantor._responseQueue = ["Inventory"]
	tantor._connectNbr("1")
	assert tantor._responseQueue == ["Inventory"]
	try:
		tantor._connectNbr("Fail")
	except SystemExit as SE:
		pass

def testPrepareElevation():
	tantor = Trantorians()
	tab = tantor._prepareElevation()
	assert tantor._isManager == True
	assert tab == ["elevation"]
	assert tantor._responseQueue == ["Inventory"]
	tantor._responseQueue = []
	tantor._level = 2
	tab = tantor._prepareElevation()
	assert tab == ["food"]
	assert tantor._responseQueue == ["Connect_nbr", "BroadcastSend"
	, "Inventory"]
	assert tantor._foodNb == 3
	tantor._responseQueue = ["Inventory"]
	tab = tantor._prepareElevation()
	assert tantor._responseQueue == ["Inventory", "Connect_nbr"
	, "BroadcastSend"]
	tantor._responseQueue = []
	tantor._nbPlayersSameLevel = 2
	tab = tantor._prepareElevation()
	assert tab == ["food"]
	assert tantor._responseQueue == ["BroadcastSend", "Inventory"]

def testFindStone():
	tantor = Trantorians()
	inventory = [345, 1, 0, 0, 0, 0, 0]
	tantor._toSearch = tantor._findStone(inventory)
	assert tantor._toSearch == ["elevation"]
	tantor._level = 2
	tantor._responseQueue = []
	inventory = [345, 0, 0, 0, 0, 0, 0]
	tantor._toSearch = tantor._findStone(inventory)
	assert tantor._toSearch == ["linemate", "deraumere", "sibur"]
	assert tantor._responseQueue == ["Look"]
	tantor._responseQueue = []
	inventory = [345, 1, 1, 0, 0, 0, 0]
	tantor._toSearch = tantor._findStone(inventory)
	assert tantor._toSearch == ["sibur"]
	assert tantor._responseQueue == ["Look"]

def testCheckEntries():
	tantor = Trantorians()
	tantor._checkEntry("ok")
	tantor._checkEntry("ko")
	assert tantor._unexpected == False
	tantor._checkEntry("Elevation underway")
	assert tantor._unexpected == True
	assert tantor._responseQueue == ["Incantation", "Incantation"]
	tantor._checkEntry("[player, food, food, food]")
	assert tantor._unexpected == False
	tantor._checkEntry("eject: 1")
	assert tantor._unexpected == True
	assert tantor._responseQueue[0] == "EjectReceived"
	tantor._checkEntry("message 3, mdr ")
	assert tantor._unexpected == True
	assert tantor._responseQueue[0] == "BroadcastReceived"
	tantor._checkEntry("Current level: 2")
	assert tantor._unexpected == False
	tantor._checkEntry("dead")
	assert tantor._unexpected == True
	assert tantor._responseQueue[0] == "dead"
	tantor._checkEntry("3")
	assert tantor._unexpected == False
	try:
		tantor._checkEntry("Test")
	except SyntaxError as SE:
		assert SE.msg == "The received command is invalid: Test"
	try:
		tantor._checkEntry("Current Level: 29")
	except SyntaxError as SE:
		assert SE.msg == "The received command is invalid: \
Current Level: 29"

def testSearchItem():
	tantor = Trantorians()
	inventory = [345, 1, 0, 0, 0, 0, 0]
	tantor._searchItem("food", inventory)
	assert tantor._toSearch == ["food"]
	assert tantor._responseQueue == ["Look"]
	inventory = [345, 0, 0, 0, 0, 0, 0]
	tantor._responseQueue = []
	tantor._searchItem("stones", inventory)
	assert tantor._toSearch == ["linemate"]
	assert tantor._responseQueue == ["Look"]

def testInventory():
	tantor = Trantorians()
	inventory = "[food 2, linemate 2, deraumere 0, sibur 1, mendiane 0, \
phiras 2, thystame 0 ]"
	tantor._inventory(inventory)
	assert tantor._toSearch == ["food"]
	assert tantor._responseQueue == ["Look"]
	tantor._responseQueue = []
	tantor._toSearch = []
	tantor._isWaiting = True
	inventory = "[food 10, linemate 0, deraumere 0, sibur 0, mendiane 0,\
phiras 0, thystame 0 ]"
	tantor._inventory(inventory)
	assert tantor._responseQueue == ["Inventory"]
	tantor._responseQueue = []
	tantor._toSearch = []
	tantor._isWaiting = False
	tantor._foodNb = 1
	tantor._inventory(inventory)
	assert tantor._foodNb == 0
	assert tantor._toSearch == ["food"]
	assert tantor._responseQueue == ["Look"]
	tantor._responseQueue = []
	tantor._toSearch = ["elevation"]
	tantor._inventory(inventory)
	assert tantor._responseQueue == ["Look"]
	tantor._responseQueue = []
	tantor._toSearch = []
	tantor._inventory(inventory)
	assert tantor._toSearch == ["linemate"]

def testFindAnotherWay():
	tantor = Trantorians()
	tantor._findAnotherWay()
	ex = [["Left", "Forward", "Inventory"]]
	ex.append(["Right", "Forward", "Inventory"])
	ex.append(["Left", "Left", "Forward", "Inventory"])
	assert tantor._responseQueue == ex[0] \
	or tantor._responseQueue == ex[1] or tantor._responseQueue == ex[2]

def testAccessItem():
	tantor = Trantorians()
	ref = ["Take food"]
	tantor._accessItem(0, 0, "food", 1)
	assert tantor._responseQueue == ref
	tantor._responseQueue = []
	ref = ["Forward", "Take food"]
	tantor._accessItem(1, 0, "food", 1)
	assert tantor._responseQueue == ref
	tantor._responseQueue = []
	ref = ["Forward", "Left", "Forward", "Take food", "Take food"]
	tantor._accessItem(1, -1, "food", 2)
	assert tantor._responseQueue == ref
	tantor._responseQueue = []
	ref = ["Forward", "Right", "Forward", "Take food"]
	tantor._accessItem(1, 1, "food", 1)
	assert tantor._responseQueue == ref
	tantor._responseQueue = []
	ref = ["Forward", "Forward", "Forward", "Left", "Forward", "Forward"]
	ref.append("Forward")
	ref.append("Take food")
	tantor._accessItem(3, -3, "food", 1)
	assert tantor._responseQueue == ref

def testFindItem():
	tantor = Trantorians()
	answer = "[player, food, , food]"
	ref = ["Forward", "Left", "Forward", "Take food"]
	tantor._findItem(answer, "food")
	assert tantor._responseQueue == ref
	tantor._responseQueue = []
	answer = "[player, food, , food, linemate, food,,, food]"
	ref = ["Forward", "Forward", "Left", "Forward", "Forward"
	, "Take linemate"]
	tantor._findItem(answer, "linemate")
	assert tantor._responseQueue == ref

def testHandleElevation():
	tantor = Trantorians()
	tantor._level = 2
	answer = "player player player, food, , food"
	tantor._handleElevation(answer)
	assert tantor._responseQueue == ["Eject", "Inventory"]
	tantor._responseQueue = []
	answer = "player player, food, , food"
	tantor._handleElevation(answer)
	assert tantor._responseQueue == ["Set linemate", "Set deraumere"
	, "Set sibur", "Incantation"]

def testLook():
	tantor = Trantorians()
	tantor._level = 2
	tantor._toSearch = ["elevation"]
	answer = "[player player player, food, , food]"
	tantor._look(answer)
	assert tantor._responseQueue == ["Eject", "Inventory"]
	tantor._toSearch = ["elevation"]
	tantor._responseQueue = []
	answer = "[player player, food, , food]"
	tantor._look(answer)
	assert tantor._responseQueue == ["Set linemate", "Set deraumere"
	, "Set sibur", "Incantation"]
	tantor._toSearch = ["food"]
	tantor._responseQueue = []
	answer = "[player player, food, , food]"
	tantor._look(answer)
	assert tantor._responseQueue == ["Forward", "Left", "Forward"
	, "Take food"]
	tantor._toSearch = ["food"]
	tantor._responseQueue = []
	answer = "[player,,,]"
	tantor._look(answer)
	refs = [["Left", "Forward", "Inventory"]]
	refs.append(["Right", "Forward", "Inventory"])
	refs.append(["Left", "Left", "Forward", "Inventory"])
	assert tantor._responseQueue == refs[0] \
	or tantor._responseQueue == refs[1] or tantor._responseQueue == refs[2]

def testAction():
	tantor = Trantorians()
	assert tantor._action("tmp")
	assert tantor._responseQueue == ["Inventory"]
	tantor._responseQueue = ["Test"]
	assert tantor._action("tmp")
	assert tantor._responseQueue == ["Test"]

def testBroadcastReset():
	tantor = Trantorians()
	tantor._level = 2
	tantor._awaitedPlayer = 1
	msg = "Fake msg"
	line = msg.split(",")
	tantor._broadcastReset(line)
	assert tantor._awaitedPlayer == 1
	msg = "message 0,yolo,Reset,1529758888.605878 11,1529758894.5255148"
	line = msg.split(",")
	tantor._broadcastReset(line)
	assert tantor._awaitedPlayer == 1

def testBroadcastLevelAsk():
	tantor = Trantorians()
	tantor._level = 2
	tantor._awaitedPlayer = 1
	msg = "message 0,yolo,Level 2,1529758888.605878 11,1529758894.5255148\
 10"
	line = msg.split(",")
	tantor._broadcastLevelAsk(line)
	assert tantor._responseQueue == ["BroadcastSend", "Inventory"]
	msg = "Fake msg"
	line = msg.split(",")
	tantor._broadcastLevelAsk(line)
	assert tantor._responseQueue == ["BroadcastSend", "Inventory"]
	msg = "message 0,yolo,Level,1529758888.605878 11,1529758894.5255148"
	line = msg.split(",")
	tantor._broadcastLevelAsk(line)
	assert tantor._onlyFood == False

def testBroadcastOnPosition():
	tantor = Trantorians()
	tantor._broadcastOnPosition("Test")
	assert tantor._awaitedPlayer == 0
	msg = "message 0,yolo,Here,1529758888.605878 11,1529758894.5255148 10,\
1529758905.054656"
	line = msg.split(",")
	tantor._broadcastOnPosition(line)
	assert tantor._awaitedPlayer == 0

def testEject():
	tantor = Trantorians()
	tantor._awaitedPlayer = 1
	tantor._isWaiting = True
	tantor._isManager = True
	tantor._nbPlayersSameLevel = 1
	tantor._inIncantation = True
	tantor._playerForIncantation = ["Trantor"]
	tantor._onlyFood = True
	tantor._eject("Tmp")
	assert tantor._awaitedPlayer == 0
	assert tantor._isWaiting == False
	assert tantor._isManager == False
	assert tantor._nbPlayersSameLevel == 0
	assert tantor._inIncantation == False
	assert tantor._playerForIncantation == []
	assert tantor._onlyFood == False

def testResetTrantorians():
	tantor = Trantorians()
	tantor._awaitedPlayer = 1
	tantor._isWaiting = True
	tantor._isManager = True
	tantor._nbPlayersSameLevel = 1
	tantor._inIncantation = True
	tantor._playerForIncantation = ["Trantor"]
	tantor._onlyFood = True
	tantor._resetTrantorians()
	assert tantor._awaitedPlayer == 0
	assert tantor._isWaiting == False
	assert tantor._isManager == False
	assert tantor._nbPlayersSameLevel == 0
	assert tantor._inIncantation == False
	assert tantor._playerForIncantation == []
	assert tantor._onlyFood == False

def testSetTimeCmdTeamLevel():
	tantor = Trantorians()
	tmp = time()
	tantor.setTimeCmdTeamLevel(tmp)
	assert tantor._timeCmdTeamLevel == tmp
