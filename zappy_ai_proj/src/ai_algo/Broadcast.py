#!/usr/bin/python3.6

from time import time

class BroadcastAi:
	"""Tools used to send message with a specific protocol"""

	@classmethod
	def requestTeamPlayerSameLevel(cls, trantorians):
		"""Send a request.

		Request to know how many players are of my team and my level.

		Args:
			trantorians: Sender of the message.

		Return:
			Return the request string
		"""

		timeCmd = time()
		request = trantorians.getTeamName() + ",Level "
		request += str(trantorians.getLevel()) + ","
		request += str(trantorians.getTimeConnect()) + " "
		request += str(trantorians.getClientNum()) + ","
		request += str(timeCmd)
		trantorians.setTimeCmdTeamLevel(timeCmd)
		return request

	@classmethod
	def answerOnPosition(cls, trantorians, id, timestamp):
		"""Send a request.

		Request to tell the chief AI that we're on the same tile

		Args:
			trantorians: Sender of the message.
			id: Id of the command we're responding to
			timestamp: Timestamp to append at the end of the cmd

		Return:
			Return the request string
		"""

		request = trantorians.getTeamName() + ",Here," + id + ","
		request += str(trantorians.getTimeConnect()) + " "
		request += str(trantorians.getClientNum()) + ","
		request += timestamp
		return request

	@classmethod
	def answerTeamPlayerSameLevel(cls, trantorians, id, timestamp):
		"""Answer a request.

		Answer if i'm the level and a memeber of the team.

		Args:
			trantorians: Sender of the message.
			id: trantorians id to who send the message.

		Return:
			Return the answer string
		"""

		request = trantorians.getTeamName() + ",Iamlevel "
		request += str(trantorians.getLevel()) + ","
		request += id + ","
		request += str(trantorians.getTimeConnect()) + " "
		request += str(trantorians.getClientNum()) + ","
		request += str(timestamp)
		return request

	@classmethod
	def requestMove(cls, trantorians):
		"""Request the others trantorians to move to him.

		Args:
			trantorians: Sender of the message.

		Return:
			Return the request string
		"""

		request = trantorians.getTeamName() + ",Move "
		request += str(trantorians.getLevel()) + ","
		request += str(trantorians.getTimeConnect()) + " "
		request += str(trantorians.getClientNum()) + ","
		for player in trantorians.getPlayerForIncantation():
			request += player[0] + " "
			request += player[1] + ":"
		request = request[:-1]
		request += "," + str(time())
		return request

	@classmethod
	def requestReset(cls, trantorians):
		"""Request the others trantorians to reset.

		Args:
			trantorians: Sender of the message.

		Return:
			Return the request string
		"""

		request = trantorians.getTeamName() + ",Reset "
		request += str(trantorians.getLevel()) + ","
		request += str(trantorians.getTimeConnect()) + " "
		request += str(trantorians.getClientNum()) + ","
		for player in trantorians.getPlayerForIncantation():
			request += player[0] + " "
			request += player[1] + ":"
		request = request[:-1]
		request += "," + str(time())
		return request

	@classmethod
	def answerAcceptYou(cls, trantorians, trantoriansID):
		"""Request the others trantorians to reset.

		Args:
			trantorians: Sender of the message.

		Return:
			Return the request string
		"""

		request = trantorians.getTeamName() + ",Accept,"
		request += trantoriansID[0] + " "
		request += trantoriansID[1] + ","
		request += str(trantorians.getTimeConnect()) + " "
		request += str(trantorians.getClientNum()) + ","
		request += str(time())
		return request