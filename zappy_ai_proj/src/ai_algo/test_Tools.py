#!/usr/bin/python3.6

from numpy import array_equal
from Tools import Tools

def testCleanLookWithSimplesBrackets():
	string = "[ player, food, food, food ]"
	string = Tools._cleanLook(string)
	assert string == "player, food, food, food"

def testCleanLookWithSpacedOutBrackets():
	string = "[player, food, , food]"
	string = Tools._cleanLook(string)
	assert string == "player, food, , food"

def testCleanInventoryWithoutHavingToSortIt():
	tabRef = [345, 3, 0, 3, 2, 5, 4]
	answer = "[ food 345, linemate 3, deraumere 0, sibur 3,"
	answer += " mendiane 2, phiras 5, thystame 4 ]"
	tabRet = Tools._cleanInventory(answer)
	assert array_equal(tabRef, tabRet)
	answer = "[food 345 , sibur 3 , phiras 5 , linemate 3 , deraumere 0,"
	answer += " mendiane 2, thystame 4]"
	tabRet = Tools._cleanInventory(answer)
	assert array_equal(tabRef, tabRet)

def testCleanInventoryAndSortIt():
	tabRef = [345, 3, 0, 3, 2, 5, 4]
	answer = "[ food 345 , sibur 3 , phiras 5 , linemate 3 , deraumere 0,"
	answer += " mendiane 2, thystame 4 ]"
	tabRet = Tools._cleanInventory(answer)
	assert array_equal(tabRef, tabRet)
	answer = "[food 345 , sibur 3 , phiras 5 , linemate 3 , deraumere 0,"
	answer += " mendiane 2, thystame 4]"
	tabRet = Tools._cleanInventory(answer)
	assert array_equal(tabRef, tabRet)

def testConvertTileZeroToSeq():
	tile = 0
	rank, dist = Tools._convertTileToSeq(tile)
	assert rank == 0
	assert dist == 0

def testConvertTileFirstToSeq():
	tile = 1
	rank, dist = Tools._convertTileToSeq(tile)
	assert rank == 1
	assert dist == -1

def testConvertTileThirdToSeq():
	tile = 3
	rank, dist = Tools._convertTileToSeq(tile)
	assert rank == 1
	assert dist == 1

def testConvertTileFifteenthToSeq():
	tile = 15
	rank, dist = Tools._convertTileToSeq(tile)
	assert rank == 3
	assert dist == 3
