/*
** EPITECH PROJECT, 2018
** PSU_zappy_2017
** File description:
** select c layer
*/
/**
* \file
* \brief Contains one of the most important function. Waiting for
* server responses.
*/

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include "ai.h"
#include "error.h"
#include "utils.h"
#include "buffer.h"

static int init_fdsets(int sfd, fd_set *rd)
{
	FD_ZERO(rd);
	FD_SET(sfd, rd);
	return (0);
}

static int check_fd_validity(int sfd)
{
	if (sfd <= 0)
		return (-1);
	if (write(sfd, "", 0) == -1) {
		saved_fd(-2);
		return (-1);
	}
	return (0);
}

/**
* \brief check if server is available to reveice data AND if serv have
** send data. If so, stack all full responses.
* \return Response on 8 bits. First 4 are dedicated to a write bit maks,
** 4 last to the number of full responses read from the server.
*/
int is_available(void)
{
	fd_set read_set;
	int ret = 0;
	int sfd = saved_fd(-1);

	if (check_fd_validity(sfd) == -1)
		return (-1);
	init_fdsets(sfd, &read_set);
	if (check_fd_validity(sfd) == -1)
		return (-1);
	if (select(sfd + 1, &read_set, NULL, NULL, NULL) == -1)
		return (utils_perror_reti("zappy: select:", -1));
	if (FD_ISSET(sfd, &read_set))
		ret = get_info_from_serv(sfd);
	if (ret == -1)
		return (-1);
	if (ret == 0)
		return (is_available());
	return (ret);
}