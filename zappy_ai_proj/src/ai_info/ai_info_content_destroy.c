/*
** EPITECH PROJECT, 2018
** PSU_zappy_2017
** File description:
** function destroying struct ai_info
*/
/**
* \file
* \brief Destroy the content of the network layer ressources
*/

#include <unistd.h>
#include "ai.h"
#include "utils.h"
#include "error.h"

/**
* \brief init the ai_info struct
* \param[out] the ai_info structure. Must be allocted.
* \return 0 on success. -1 on error, error is printed on error output.
*/
int ai_info_content_destroy(ai_info_t *info)
{
	if (info == NULL)
		return (utils_error_reti(ERR_CL_CONTENT_DESTROY, ERR_RET));
	info->team = NULL;
	if (info->connection)
		free(info->connection);
	info->connection = NULL;
	return (0);
}