/*
** EPITECH PROJECT, 2018
** PSU_zappy_2017
** File description:
** function initialysing struct ai_info
*/
/**
* \file
* \brief This file contains the function initializing the client network layer
*/

#include <unistd.h>
#include "ai.h"
#include "utils.h"

/**
* \brief init the ai_info struct
* \param[out] the ai_info structure. Must be allocted.
* \return 0 on success. -1 on error, error is printed on error output.
*/
int ai_info_content_init(ai_info_t *info)
{
	if (info == NULL)
		return (utils_error_reti(ERR_CL_CONTENT_INIT, ERR_RET));
	info->team = NULL;
	info->connection = malloc(sizeof(*(info->connection)));
	if (info->connection == NULL)
		return (utils_perror_reti(ERR_CL_INIT_ALLOC, ERR_RET));
	info->connection->port = 0;
	info->connection->hostname = NULL;
	return (0);
}