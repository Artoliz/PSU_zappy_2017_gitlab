/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Define Ai's Help Function
*/
/**
* \file
* \brief Contains the usage printring function
*/

#include <stdio.h>
#include "ai.h"

/**
* \brief This function display the help message of the ai
* \param[in] BINARY The binary name
* \return 0
*/
int display_ai_help_message(const char *binary)
{
	printf(AI_USAGE, binary);
	printf(AI_PORT);
	printf(AI_TEAM);
	printf(AI_HOSTNAME);
	return (0);
}