/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Main AI
*/
/**
* \file
* \brief Contains one of the most important function. Connection
* with the server.
*/

#include <string.h>
#include <stdio.h>
#include "ai.h"
#include "error.h"

/**
* \brief main connection function
* \param[in] number of arguments
* \param[in] arguments of the program
* \return 0 on success. -1 on fail.
*/
int main_connection(int argc, char const **argv)
{
	ai_info_t info;

	if (ai_info_content_init(&info) == ERR_RET)
		return (ERR_RET);
	if (retrieve_args(argc - 1, argv + 1, &info) == ERR_RET)
		return (ERR_RET);
	if (connect_to_serv(&info) == ERR_RET) {
		ai_info_content_destroy(&info);
		return (ERR_RET);
	}
	saved_fd(info.connection->sfd);
	ai_info_content_destroy(&info);
	return (0);
}