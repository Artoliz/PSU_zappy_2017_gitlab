/*
** EPITECH PROJECT, 2018
** PSU_zappy_2017
** File description:
** connection client function
*/
/**
* \file
* \brief File containing the connection to server function
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "ai.h"
#include "error.h"
#include "utils.h"

static int establish_connection(const ai_info_t *info, struct addrinfo *result)
{
	int sfd = 0;

	if (!result)
		return (ERR_RET);
	for (struct addrinfo *tmp = result; tmp; tmp = tmp->ai_next) {
		sfd = socket(tmp->ai_family, tmp->ai_socktype,
		tmp->ai_protocol);
		if (sfd == ERR_RET)
			continue;
		setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &(int){1},
		sizeof(int));
		((struct sockaddr_in *)tmp->ai_addr)->sin_port = htons(
		info->connection->port);
		if (connect(sfd, tmp->ai_addr, tmp->ai_addrlen) != ERR_RET)
			return (sfd);
		close(sfd);
	}
	fprintf(stderr, ERR_CL_CONNECTION, info->connection->hostname, \
	info->connection->port);
	return (ERR_RET);
}

/**
* \brief allow connection to a serv depending on arguments given
* \param[in - out] structure containing informations relative to the client
* \return 0 on succes. -1 on error, error is printed on error output.
*/
int connect_to_serv(ai_info_t *info)
{
	struct addrinfo hints;
	struct addrinfo *result = NULL;
	int ret = 0;

	if (!info || !info->connection)
		return (utils_error_reti(CL_INV_CO_NO_INFO, ERR_RET));
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = 0;
	hints.ai_protocol = IPPROTO_TCP;
	ret = getaddrinfo(
	info->connection->hostname, NULL, &hints, &result);
	if (ret != 0) {
		fprintf(stderr, ERR_CL_GETADDR, gai_strerror(ret));
		return (ERR_RET);
	}
	info->connection->sfd = establish_connection(info, result);
	freeaddrinfo(result);
	return (info->connection->sfd > 2 ? 0 : ERR_RET);
}