/*
** EPITECH PROJECT, 2018
** PSU_zappy_2017
** File description:
** stacks of commands responses from server
*/
/**
* \file
* \brief Contains a function adding a response to the responses list
*/

#include "utils.h"
#include "ai.h"
#include "error.h"

/**
* \brief add an element to the stack
* \param[in] pointer throught the string which will be copied
* \return 1 on success. -1 on error.
*/
int ai_cmdlist_add_element(char const *string)
{
	char **head = NULL;

	if (!string)
		return (ERR_RET);
	head = ai_cmdlist(NULL);
	head = utils_wt_push(head, string);
	if (!head)
		return (ERR_RET);
	ai_cmdlist(head);
	return (1);
}