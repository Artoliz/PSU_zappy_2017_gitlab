/*
** EPITECH PROJECT, 2018
** PSU_zappy_2017
** File description:
** stacks of commands responses from server
*/
/**
* \file
* \brief Contains the access function of the command list
*/

#include "utils.h"

/**
* \brief access the statically stocked address of the all stack of respones
** from the server
* \param[in] If not null, set the static pointer to this address.
* \return The address of the allocated array
*/
char **ai_cmdlist(char **ai_cmdlist_h)
{
	static char **ai_cmdlist_head = NULL;

	if (ai_cmdlist_h)
		ai_cmdlist_head = ai_cmdlist_h;
	return (ai_cmdlist_head);
}